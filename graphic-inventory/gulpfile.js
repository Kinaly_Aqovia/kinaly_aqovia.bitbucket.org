'use strict';

var gulp = require('gulp');
var plugins = require('gulp-load-plugins')({
  pattern: ['gulp-*', 'gulp.*'],
  replaceString: /\bgulp[\-.]/,
  lazy: true
});

// Paths and config variables
var source = './_src',
    prod = './_dist',
    autoprefixerOptions = { browsers: ['last 3 versions', '> 5%', 'Firefox ESR'] };


// Export Sketch assets
gulp.task('sketch', function() {
  return gulp
    .src(source + '/*.sketch')
    .pipe(plugins.sketch({
      export: 'slices',
      format: 'png',
      saveForWeb: true,
      scales: 1.0,
      trimmed: false
    }))
    .pipe(gulp.dest(prod + '/img'))
    .pipe(plugins.notify({ message: 'Sketch task complete' }));
})


// Create styles
gulp.task('styles', function () {
  return gulp
    .src(source + '/less/[!_]*.less')
    .pipe(plugins.sourcemaps.init())
    .pipe(plugins.less())
    .pipe(plugins.autoprefixer(autoprefixerOptions))
    .pipe(plugins.sourcemaps.write('../css'))
    .pipe(gulp.dest(prod + '/css'))
    .pipe(plugins.notify({ message: 'Styles task complete' }));
});


// Copy JS
gulp.task('js', function() {
  return gulp.src(source + '/js/*.js')
    .pipe(gulp.dest(prod + '/js/'))
    .pipe(plugins.notify({ message: 'Copy JS task complete' }));
});


// Copy libraries
gulp.task('copyLib', function() {
  return gulp.src(source + '/lib/**/*')
    .pipe(gulp.dest(prod + '/lib'))
    .pipe(plugins.notify({ message: 'Copy libraries task complete' }));
});


// Copy images
gulp.task('images', function() {
  return gulp.src(source + '/img/*.{png,jpg,jpeg,gif,svg}')
    .pipe(plugins.imagemin())
    .pipe(gulp.dest(prod + '/img'))
    .pipe(plugins.notify({ message: 'Images task complete' }));
});


// Copy fonts in CSS folder
gulp.task('fonts', function() {
  return gulp.src(source + '/fonts/**/*')
    .pipe(gulp.dest(prod + '/css/fonts'))
    .pipe(plugins.notify({ message: 'Copy fonts task complete' }));
});



// Nunjucks
gulp.task('nunjucks', function() {
  // Gets .html and .nunjucks files in pages
  return gulp.src(source + '/pages/**/*.html')
  // Adding data to Nunjucks
	.pipe(plugins.data(function() {
	  return require(source + '/data.json')
	}))
  // Renders template with nunjucks
  .pipe(plugins.nunjucksRender({
      path: [source + '/templates']
    }))
  // output files in app folder
  .pipe(gulp.dest(prod))
  .pipe(plugins.notify({ message: 'HTML template task complete' }));
});


// Default task
gulp.task('default', function() {
    gulp.start('styles', 'js', 'copyLib', 'images', 'nunjucks');
});


// Watch
gulp.task('watch', function () {
  // Watch sketch files
  gulp.watch(source + '/*.sketch', ['sketch']);

  // Watch .less files
  gulp.watch(source + '/less/**/*.less', ['styles']);

  // Watch js
  gulp.watch(source + '/js/**/*', ['js']);

  // Watch libraries
  gulp.watch(source + '/lib/**/*', ['copyLib']);

  // Watch templates
  gulp.watch(source + '/templates/**/*.html', ['nunjucks']);
  gulp.watch(source + '/pages/**/*.html', ['nunjucks']);

  // Create LiveReload server
  plugins.livereload.listen();

  // Watch any files in dist/, reload on change
  gulp.watch([prod + '/**']).on('change', plugins.livereload.changed);
});
