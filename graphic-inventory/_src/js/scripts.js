        // from Ditching jQuery

// https://gomakethings.com/ditching-jquery/#manipulate-height
/**
 * Get the height of an element
 * @param  {Node}   elem The element
 * @return {Number}      The height
 */
var getHeight = function(elem) {
  return Math.max(elem.scrollHeight, elem.offsetHeight, elem.clientHeight);
};



// https://gomakethings.com/ditching-jquery/#climb-up-the-dom
/**
 * Get the closest matching element up the DOM tree.
 * @private
 * @param  {Element} elem     Starting element
 * @param  {String}  selector Selector to match against
 * @return {Boolean|Element}  Returns null if not match found
 */
var getClosest = function(elem, selector) {
  // Element.matches() polyfill
  if (!Element.prototype.matches) {
    Element.prototype.matches =
      Element.prototype.matchesSelector ||
      Element.prototype.mozMatchesSelector ||
      Element.prototype.msMatchesSelector ||
      Element.prototype.oMatchesSelector ||
      Element.prototype.webkitMatchesSelector ||
      function(s) {
        var matches = (this.document || this.ownerDocument).querySelectorAll(s),
          i = matches.length;
        while (--i >= 0 && matches.item(i) !== this) {}
        return i > -1;
      };
  }

  // Get closest match
  for (; elem && elem !== document; elem = elem.parentNode) {
    if (elem.matches(selector)) return elem;
  }

  return null;
};




var allPortFinders = document.querySelectorAll(".port-finder");
for (var i = 0; i < allPortFinders.length; i++) {
  allPortFinders[i].addEventListener("click", portFinderActions);
}

function portFinderActions(e) {
  if (e.target.classList.contains("port-finder__unselect-item") == true) {
    updateSectionVisibility(e.target);
  }
}


var allInputs = document.querySelectorAll(".port-finder__item");
for (var i = 0; i < allInputs.length; i++) {
  allInputs[i].addEventListener("change", inputActions);
}

function inputActions(e) {
  // setTimeo
  updatePortFinderHeader(e.target);
  updateSectionVisibility(e.target);
}


var setSectionsHeight = document.querySelectorAll(".port-finder__section");
for (var i = 0; i < setSectionsHeight.length; i++) {
  if (i == 0) {
    setSectionsHeight[i].style.height = family(setSectionsHeight[i]).innerHeader.offsetHeight + 1 + getHeight(family(setSectionsHeight[i]).sectionDetails) + "px";
  } else {
    setSectionsHeight[i].style.height = 0;
  }
}



function clearSection(elem) {
  var allInputs = elem.querySelectorAll(".custom-choice-input__input");
  uncheckItems(allInputs);
  updatePortFinderHeader(elem);
}



function getClickedInput(array) {
  for (var i = 0; i < array.length; i++) {
    if (array[i].checked == true) return array[i];
  }
}


function uncheckItems(array) {
  for (var i = 0; i < array.length; i++) {
    array[i].checked = false;
  }
}



function family(elem) {
  var section = elem.classList.contains("port-finder__section") == true ? elem : getClosest(elem, ".port-finder__section");
  
  var parts = {
    "section": section,
    "label": section.querySelector(".port-finder__selected-item"),
    "unselectBtn": section.querySelector(".port-finder__unselect-item"),
    "expandableArea": section.querySelector(".port-finder__expandable-area"),
    "sectionDetails": section.querySelector(".port-finder__section-details"),
    "innerHeader": section.querySelector(".port-finder__inner-header"),
    "header": section.querySelector(".port-finder__section-header")
  }
  return parts;
}



function findCheckedInput(array) {
  var checkedItem = [];
  
  for (var i = 0; i < array.length; i++) {
    if (array[i].checked == true) {
      checkedItem.push(array[i]); 
    }
  }
  
  if (checkedItem.length > 1) { return checkedItem } else { return checkedItem[0] };
}



function updatePortFinderHeader(elem) {
  var targetSection = family(elem).section;
  var headerLabel = family(elem).label;
  var unselectBtn = family(elem).unselectBtn;
  
  var inputs = targetSection.querySelectorAll(".port-finder__item > .custom-choice-input__input");
  var checkedInput = findCheckedInput(inputs);
  
  var timeout = (checkedInput != undefined) ? 0 : 350;
  
  setTimeout(
    function() {
      // set the text of the label to the checked input value or to nothing if no checked input
      headerLabel.textContent = (checkedInput != undefined) ? checkedInput.value : "";
    }
  , timeout);
  
  // hide or display the uncheck button depending on the presence of a checked input
  if (checkedInput != undefined) {
    headerLabel.classList.remove("-is-hidden");
    unselectBtn.classList.remove("-is-hidden");
  } else {
    headerLabel.classList.add("-is-hidden");
    unselectBtn.classList.add("-is-hidden");
  }
}


function updateSectionVisibility(elem) {
  var allSections = document.querySelectorAll(".port-finder__section");
  var targetSection = family(elem).section;
  
  // update visibility when unselect button is clicked
  if (elem.classList.contains("port-finder__unselect-item") == true) {
    var marker;
    // identify in which section the unselect button has been clicked and set it to current
    for (var i = 0; i < allSections.length; i++) {
      if (allSections[i] == targetSection) {
        marker = i;
        allSections[i].classList.add("-is-current");
        allSections[i].classList.remove("-is-active");
        
        allSections[i].classList.add("-is-open");
        allSections[i].classList.remove("-is-half-open");
      }
    }
    
    // remove the active or current class from all subsequent sections
    for (var i = marker + 1; i < allSections.length; i++) {
      allSections[i].classList.remove("-is-current");
      allSections[i].classList.remove("-is-active");
      
      allSections[i].classList.remove("-is-open");
      allSections[i].classList.remove("-is-half-open");
    }
    
    // uncheck all inputs from the section that had the unselect button clicked 
    for (var i = marker; i < allSections.length; i++) {
      clearSection(allSections[i]);
    }
  
  // update visibility when an input is checked
  } else {
    // family(targetSection).expandableArea.style = 0;
    var marker;
    // identify in which section the input has been clicked
    for (var i = 0; i < allSections.length; i++) {
      if (allSections[i] == targetSection) {
        marker = i;
        allSections[i].classList.remove("-is-current");
        allSections[i].classList.add("-is-active");
        
        allSections[i].classList.remove("-is-open");
        allSections[i].classList.add("-is-half-open");
      }
    }
    // display the next section by setting the class to current
    if (allSections.length > marker + 1) {
      allSections[marker + 1].classList.add("-is-current");
      allSections[marker + 1].classList.remove("-is-active");
      
      allSections[marker + 1].classList.add("-is-open");
      allSections[marker + 1].classList.remove("-is-half-open");
    }
    
    // hide all sections above the current one
    for (var i = marker + 2; i < allSections.length; i++ ) {
      allSections[i].classList.remove("-is-active");
      allSections[i].classList.remove("-is-current");
      
      allSections[i].classList.remove("-is-open");
      allSections[i].classList.remove("-is-half-open");
    }
    
    // uncheck all inputs from the next section to the end
    for (var i = marker + 1; i < allSections.length; i++) {
      clearSection(allSections[i]);
    }
  }
  
  var visibleSections = document.querySelectorAll(".port-finder__section");
  for (var i = 0; i < visibleSections.length; i++) {
    var height = 0;
    
    if (visibleSections[i].classList.contains("-is-open") == true) {
      height = family(visibleSections[i]).innerHeader.offsetHeight + 1 + getHeight(family(visibleSections[i]).sectionDetails) + "px";
    
    } else if (visibleSections[i].classList.contains("-is-half-open") == true) {
      var elem = family(visibleSections[i]).header;
      height = family(visibleSections[i]).innerHeader.offsetHeight + 1 + "px";
    
    } else {
      height = 0;
    }
    
    visibleSections[i].style.height = height;
  }
}