/**
 * Get the closest matching element up the DOM tree.
 * @private
 * @param  {Element} elem     Starting element
 * @param  {String}  selector Selector to match against
 * @return {Boolean|Element}  Returns null if not match found
 */
var getClosest = function ( elem, selector ) {

    // Element.matches() polyfill
    if (!Element.prototype.matches) {
        Element.prototype.matches =
            Element.prototype.matchesSelector ||
            Element.prototype.mozMatchesSelector ||
            Element.prototype.msMatchesSelector ||
            Element.prototype.oMatchesSelector ||
            Element.prototype.webkitMatchesSelector ||
            function(s) {
                var matches = (this.document || this.ownerDocument).querySelectorAll(s),
                    i = matches.length;
                while (--i >= 0 && matches.item(i) !== this) {}
                return i > -1;
            };
    }

    // Get closest match
    for ( ; elem && elem !== document; elem = elem.parentNode ) {
        if ( elem.matches( selector ) ) return elem;
    }

    return null;

};



// Show an element
var show = function(elem) {
  // Get the natural height of the element
  var getNaturalHeight = function() {
    elem.style.display = "block"; // Make it visible
    var height = elem.scrollHeight + "px"; // Get it's height
    elem.style.display = null; //  Hide it again
    return height;
  }; 
  
  // fetch computed transition duration
  var duration = getTransitionDuration(elem) != 0 ? getTransitionDuration(elem) : 350;

  var height = getNaturalHeight(); // Get the natural height
  elem.classList.add("-is-open"); // Make the element visible
  if (family(elem).mainItem != undefined) {
  	family(elem).mainItem.classList.add("-is-open");
  }
  elem.style.height = height; // Update the max-height
  elem.style.borderTopWidth = null; // for IE 11 to properly reduce to 0
  elem.style.borderBottomWidth = null; // for IE 11 to properly reduce to 0

  // Once the transition is complete, remove the inline max-height so the content can scale responsively
  window.setTimeout(function() {
    elem.style.height = "";
  }, duration);
};

// Hide an element
var hide = function(elem) {
  // Give the element a height to change from
  elem.style.height = elem.scrollHeight + "px";
  
  // fetch computed transition duration
  var duration = getTransitionDuration(elem) != 0 ? getTransitionDuration(elem) : 350;
  
  // Set the height back to 0
  window.setTimeout(function() {
    elem.style.height = "0";
    elem.style.borderTopWidth = 0; // for IE 11 to properly reduce to 0
    elem.style.borderBottomWidth = 0; // for IE 11 to properly reduce to 0
  }, 1);

  // When the transition is complete, hide it
  window.setTimeout(function() {
    elem.classList.remove("-is-open");
  }, duration);

  if (family(elem).mainItem != undefined) {
  	family(elem).mainItem.classList.remove("-is-open");
  }
};

// Toggle element visibility
var toggle = function(elem, timing) {
  // If the element is visible, hide it
  if (elem.classList.contains("-is-open")) {
    hide(elem);
    return;
  }

  // Otherwise, show it
  show(elem);
};


var getTransitionDuration = function(elem) {
  // fetch properties that are transitioned
  var transitionProperty = window.getComputedStyle(elem).transitionProperty.split(', ');
  
  // fetch transition duration for each property
  var durationArray = window.getComputedStyle(elem).transitionDuration.split(', ');
  
  var duration;
  
  // fetch height transition duration, or the all transition duration
  for (var i = 0; i < durationArray.length; i++) {
    if (transitionProperty[i] == "height" || transitionProperty[i] == "all") {
        duration = parseFloat(durationArray[i],10)*1000;
    }
  }
  return duration;
}



// custom scripts here
var portalMenu = document.querySelector(".portal-menu");
portalMenu.addEventListener("click", function(e) {
	event.preventDefault();
	
	portalMenuActions(e);

}, false);

function portalMenuActions(e) {
	// testShowElem(e);

	toggleSubMenu(e.target);
}


function family(elem) {
	var mainGroup = elem.classList.contains(".portal-menu__main-group") == true ? elem : getClosest(elem, ".portal-menu__main-group");

	var parts = {
		"mainGroup": mainGroup,
		"mainItem": mainGroup.querySelector(".portal-menu__main-item"),
		"subMenu": mainGroup.querySelector(".portal-menu__submenu")
	};

	return parts;
}



function testShowElem(elem) {
	console.log(elem.target, family(elem.target).subMenu);
}


function toggleSubMenu(elem) {

	var mainItem = getClosest(elem, ".portal-menu__main-item");

	if (mainItem != null && mainItem.classList.contains("-no-submenu") == false) {
		var openedSubMenus = document.querySelectorAll(".portal-menu__submenu.-is-open");
		for (var i = 0; i < openedSubMenus.length; i++) {
			hide(openedSubMenus[i]);
		}

		toggle(family(mainItem).subMenu);
	}
}

