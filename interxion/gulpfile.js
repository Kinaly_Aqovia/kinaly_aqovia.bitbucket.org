/*!
 * gulp
 * to install locally
 * $ npm install gulp gulp-sass gulp-sourcemaps gulp-uncss gulp-imagemin gulp-html-extend gulp-notify gulp-livereload gulp-autoprefixer --save-dev
 *
 * to use global packages
 * $ npm link gulp gulp-sass gulp-sourcemaps gulp-uncss gulp-imagemin gulp-html-extend gulp-notify gulp-livereload gulp-autoprefixer
 */

// Load plugins
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    uncss = require('gulp-uncss'),
    imagemin = require('gulp-imagemin'),
    extender = require('gulp-html-extend'),
    notify = require('gulp-notify'),
    livereload = require('gulp-livereload');

// Paths and config variables
var source = './_src',
    prod = './_dist',
    sassOptions = { outputStyle: 'compressed', sourcemap: true },
    autoprefixerOptions = { browsers: ['last 3 versions', '> 5%', 'Firefox ESR'] };


// Create styles
gulp.task('styles', function () {
  return gulp
    .src(source + '/sass/[!_]*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass(sassOptions).on('error', sass.logError))
    .pipe(autoprefixer(autoprefixerOptions))
    // .pipe(uncss({
    //   html: [source + '/templates/*.html'],
    //   ignore: [
    //     /is/
    //   ]
    // }))
    .pipe(sourcemaps.write('../css'))
    .pipe(gulp.dest(prod + '/css'))
    .pipe(notify({ message: 'Styles task complete' }));
});


// Copy JS
gulp.task('js', function() {
  return gulp.src(source + '/js/*.js')
    .pipe(gulp.dest(prod + '/js/'))
    .pipe(notify({ message: 'Copy JS task complete' }));
});


// Copy libraries
gulp.task('copyLib', function() {
  return gulp.src(source + '/lib/**/*')
    .pipe(gulp.dest(prod + '/lib'))
    .pipe(notify({ message: 'Copy libraries task complete' }));
});


// Copy fonts
gulp.task('fonts', function() {
  return gulp.src(source + '/fonts/**/*')
    .pipe(gulp.dest(prod + '/fonts'))
    .pipe(notify({ message: 'Copy fonts task complete' }));
});


// Copy images
gulp.task('images', function() {
  return gulp.src(source + '/img/*.{png,jpg,jpeg,gif,svg}')
    .pipe(imagemin())
    .pipe(gulp.dest(prod + '/img'))
    .pipe(notify({ message: 'Images task complete' }));
});


// Copy fonts in CSS folder
gulp.task('fonts', function() {
  return gulp.src(source + '/fonts/**/*')
    .pipe(gulp.dest(prod + '/fonts'))
    .pipe(notify({ message: 'Copy fonts task complete' }));
});


// Extend HTML
gulp.task('extend', function () {
  // extend all HTML files apart from the ones starting by __
  return gulp.src(source + '/templates/[^__]*.html')
    .pipe(extender({annotations:true,verbose:false})) // default options
    .pipe(gulp.dest(prod))
    .pipe(notify({ message: 'HTML extend task complete' }));
});


// Default task
gulp.task('default', function() {
    gulp.start('styles', 'js', 'copyLib', 'fonts', 'images', 'extend');
});


// Watch
gulp.task('watch', function () {
  // Watch .scss files
  gulp.watch(source + '/sass/**/*.scss', ['styles']);

  // Watch js
  gulp.watch(source + '/js/**/*', ['js']);

  // Watch libraries
  gulp.watch(source + '/lib/**/*', ['copyLib']);

  // Watch fonts
  gulp.watch(source + '/fonts/**/*', ['fonts']);

  // Watch templates
  gulp.watch(source + '/templates/*.html', ['extend']);

  // Create LiveReload server
  livereload.listen();

  // Watch any files in dist/, reload on change
  gulp.watch([prod + '/**']).on('change', livereload.changed);
});
