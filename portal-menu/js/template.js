// functions

/**
 * Get the closest matching element up the DOM tree.
 * @param  {Element} elem     Starting element
 * @param  {String}  selector Selector to match against (class, ID, data attribute, or tag)
 * @return {Boolean|Element}  Returns null if not match found
 */
var getClosest = function ( elem, selector ) {

    // Variables
    var firstChar = selector.charAt(0);
    var supports = 'classList' in document.documentElement;
    var attribute, value;

    // If selector is a data attribute, split attribute from value
    if ( firstChar === '[' ) {
        selector = selector.substr( 1, selector.length - 2 );
        attribute = selector.split( '=' );

        if ( attribute.length > 1 ) {
            value = true;
            attribute[1] = attribute[1].replace( /"/g, '' ).replace( /'/g, '' );
        }
    }

    // Get closest match
    for ( ; elem && elem !== document && elem.nodeType === 1; elem = elem.parentNode ) {

        // If selector is a class
        if ( firstChar === '.' ) {
            if ( supports ) {
                if ( elem.classList.contains( selector.substr(1) ) ) {
                    return elem;
                }
            } else {
                if ( new RegExp('(^|\\s)' + selector.substr(1) + '(\\s|$)').test( elem.className ) ) {
                    return elem;
                }
            }
        }

        // If selector is an ID
        if ( firstChar === '#' ) {
            if ( elem.id === selector.substr(1) ) {
                return elem;
            }
        }

        // If selector is a data attribute
        if ( firstChar === '[' ) {
            if ( elem.hasAttribute( attribute[0] ) ) {
                if ( value ) {
                    if ( elem.getAttribute( attribute[0] ) === attribute[1] ) {
                        return elem;
                    }
                } else {
                    return elem;
                }
            }
        }

        // If selector is a tag
        if ( elem.tagName.toLowerCase() === selector ) {
            return elem;
        }

    }

    return null;

};

var elem = document.querySelector( '#some-element' );
var closest = getClosest( elem, '.some-class' );


/**
 * Get the height of an element
 * @param  {Node}   elem The element
 * @return {Number}      The height
 */
var getHeight = function ( elem ) {
    return Math.max( elem.scrollHeight, elem.offsetHeight, elem.clientHeight );
};


/**
 * Get all siblings of an element
 * @param  {Node}  elem The element
 * @return {Arrat}      The siblings
 */
var getSiblings = function ( elem ) {
    var siblings = [];
    var sibling = elem.parentNode.firstChild;
    for ( ; sibling; sibling = sibling.nextSibling ) {
        if ( sibling.nodeType === 1 && sibling !== elem ) {
            siblings.push( sibling );
        }
    }
    return siblings;
};



// change the title and the content for all variants of the menu
var allPages = document.querySelectorAll('.js-page-count');

for ( var i = 0, len = allPages.length; i < len; i++ ) {
	allPages[i].addEventListener('click', function(e){
		var thisContent = this.innerHTML.trim().toLowerCase();
		for (var j = 0; j < dataObj.length; j++) {
			if (dataObj[j].menuitem.toLowerCase() == thisContent) {
				var content = dataObj[j].content;
				var pageTitle = dataObj[j].pageTitle;
				document.getElementsByClassName("page-title")[0].innerHTML = pageTitle;
				document.getElementsByClassName("generated-content")[0].innerHTML = content; 
			}
		}

		// specific to portal-menu v02
		var allActive = document.querySelectorAll('.-is-active-page');
		for (var j = 0; j < allActive.length; j++) {
			allActive[j].classList.remove('-is-active-page');
		}

		var activeMainItem = getClosest(this, '.pm02__main-item');

		this.classList.add('-is-active-page');
		if (activeMainItem != undefined) {
			activeMainItem.classList.add('-is-active-page');
		}

	});
}



// portal menu v01
var menuItemLvOne = document.querySelectorAll(".portal-menu__item.-is-level-01");

for ( var i = 0, len = menuItemLvOne.length; i < len; i++ ) {
	menuItemLvOne[i].addEventListener('click', function(e){
		if (this.parentNode.querySelector(".portal-menu__level-two") != null) {
			this.classList.toggle('-is-open');
		}

		var current = this;
	  for ( var j = 0, milone = menuItemLvOne.length; j < milone; j++ ) {
	  	if (menuItemLvOne[j] != current) {
    		menuItemLvOne[j].classList.remove( '-is-open' );
	  	}
		}
    
    var menu = document.querySelector(".portal-menu");
    var openExist = document.querySelectorAll(".portal-menu__item.-has-children.-is-open").length;
    if (openExist > 0) {
    	menu.classList.add("-showable-popup-label");
    } else {
    	menu.classList.remove("-showable-popup-label");
    }
		e.preventDefault();
  }, false);

}


var menuHeads = document.querySelectorAll(".portal-menu__level-two-head");
for ( var i = 0, len = menuHeads.length; i < len; i++ ) {
	menuHeads[i].addEventListener('click', function(e){
		var closestLvOne = getClosest( this, '.portal-menu__level-one' );
		var openMenu = closestLvOne.querySelector( '.portal-menu__item' );
		// console.log(openMenu);
		openMenu.classList.remove('-is-open');

		var menu = document.querySelector(".portal-menu");
    var openExist = document.querySelectorAll(".portal-menu__item.-has-children.-is-open").length;
    if (openExist > 0) {
    	menu.classList.add("-showable-popup-label");
    } else {
    	menu.classList.remove("-showable-popup-label");
    }
		e.preventDefault();
  }, false);
}


var sublinks = document.querySelectorAll(".portal-menu__item.-has-page");
for ( var i = 0, slen = sublinks.length; i < slen; i++ ) {
	sublinks[i].addEventListener('click', function(e){
		this.classList.add('-is-current-page');

		var current = this
		for ( var j = 0, slen2 = sublinks.length; j < slen2; j++ ) {
	  	if (sublinks[j] != current) {
    		sublinks[j].classList.remove( '-is-current-page' );
	  	}
		}


		var pastCurrentSection = document.querySelectorAll('.portal-menu__item.-is-current-section');
		for ( var j = 0, pastlen = pastCurrentSection.length; j < pastlen; j++ ) {
			pastCurrentSection[j].classList.remove('-is-current-section');
		}

		var closestLvOne = getClosest( this, '.portal-menu__level-one' );
		var closestLvOneItem = closestLvOne.querySelector('.portal-menu__item.-is-level-01');
		closestLvOneItem.classList.add('-is-current-section');

		e.preventDefault();
	}, false);
}

var noChild = document.querySelectorAll(':not(.-has-children).portal-menu__item.-is-level-01');
for ( var i = 0, nlen = noChild.length; i < nlen; i++ ) {
	noChild[i].addEventListener('click', function(e){
		
		var pastCurrentSection = document.querySelectorAll('.portal-menu__item.-is-current-section');
		for ( var j = 0, pastlen = pastCurrentSection.length; j < pastlen; j++ ) {
			pastCurrentSection[j].classList.remove('-is-current-section');
		}

		for ( var j = 0, slen2 = sublinks.length; j < slen2; j++ ) {
    	sublinks[j].classList.remove( '-is-current-page' );
		}

		this.classList.add('-is-current-section');

		e.preventDefault();
	}, false);
}



// portal menu v02
var expandableItems = document.querySelectorAll('.pm02__arrow');

for (var i = 0, len = expandableItems.length; i < len; i++) {
	var closestMainItem = getClosest(expandableItems[i], '.pm02__main-item');
	var item = closestMainItem.querySelector('.pm02__main-item-label')
	
	item.addEventListener('click', function(e){
		var closestSubMenu = getSiblings(this)[0];
		var innerMenu = closestSubMenu.querySelector('.pm02__submenu-inner');
		
		var ancestor = getClosest(this, '.pm02__main-item');
		
		// only one submenu at a time
		var previousOpen = document.querySelector('.-is-open');

		if (previousOpen != ancestor && previousOpen != null) {
			previousOpen.classList.remove('-is-open');

			for (var j = 0, len = document.querySelectorAll('.pm02__submenu').length; j < len; j++) {
				document.querySelectorAll('.pm02__submenu')[j].style.height = 0;
			}
		}
		// end of only one submenu at a time

		ancestor.classList.toggle('-is-open');

		if (ancestor.classList.contains('-is-open')) {
			closestSubMenu.style.height = getHeight(innerMenu) + "px";
		} else {
			closestSubMenu.style.height = 0;
		}
		e.preventDefault();
	}, false);
}