# palette
palette = [
  '#f2eeb1', # [0] pale yellow
  '#e1edce', # [1] pale green
  '#b7d0c1', # [2] light green
  '#8ab3b3', # [3] aqua green
  '#668391', # [4] blue-ish green
  '#3f5270', # [5] blue denim
  '#6e4c60', # [6] purple
  '#9d4651', # [7] maroon purple
  '#cf3e40', # [8] firy red
  '#d66b5f', # [9] muted red
  '#df967c', # [10] salmon orange
  '#e8c198' # [11] pale orange
]

# define a page component
page = new PageComponent
  width: Screen.width
  height: Screen.height
  scrollVertical: false
  backgroundColor: 'white'

# disable draggable page
page.content.draggable.enabled = false


# set the number of page required for the prototype (not necessarily screens)
numberOfPage = 1


# array definition to store and target pages and containers
pageArray = []
mainContainerArray = []



# loop to create the pages, the containers and any other common elements
for i in [0..numberOfPage-1]
  # create the page layers that are linked to the page component
  pageItem = new Layer
    parent: page.content
    width: Screen.width
    height: Screen.height
    x: Screen.width * i
    backgroundColor: Utils.randomChoice(palette)
    name: i

  pageItem.scroll = true
  pageItem.scrollHorizontal = false
  pageItem.mouseWheelEnabled = true

  pageArray.push(pageItem)



  # create the main containers for each page
  mainContainer = new Layer
    width: 800
    height: 200
    backgroundColor: 'white'
    superLayer: pageArray[i]

  mainContainer.centerY()

  mainContainer.style =
    width: '800px',
    marginLeft: 'calc((100% - 800px) / 2)'

  # make the container scrollable
  mainContainer.scroll = true
  mainContainer.scrollHorizontal = false
  mainContainer.mouseWheelEnabled = true

  mainContainerArray.push(mainContainer)



mainContainerArray[0].y = 20

# Case buttons container
caseBtnContainer = new Layer
  superLayer: mainContainerArray[0]
  width: 750
  height: 40
  backgroundColor: 'transparent'
  x: 10
  y: 10

caseBtnContainer.centerX()


caseBtnContainer__hands = new Layer
  superLayer: caseBtnContainer
  width: 114
  height: 24
  image: "img/btn-hands.png"

caseBtnContainer__access = new Layer
  superLayer: caseBtnContainer
  width: 136
  height: 24
  image: "img/btn-access.png"
  x: caseBtnContainer__hands.x + caseBtnContainer__hands.width + 20

caseBtnContainer__delivery = new Layer
  superLayer: caseBtnContainer
  width: 155
  height: 24
  image: "img/btn-delivery.png"
  x: caseBtnContainer__access.x + caseBtnContainer__access.width + 20



# layer that contains the case layers
caseLayersContainer = new Layer
  superLayer: mainContainerArray[0]
  width: 750
  backgroundColor: "transparent"
  height: 0
  x: 10
  y: 10

caseLayersContainer.centerX()



# add a hands and eyes case layer when click on the hands button
caseBtnContainer__hands.onClick ->
  caseLayersContainer__item = new Layer
    superLayer: caseLayersContainer
    width: caseLayersContainer.width
    name: 'caseLayer__hands'
    backgroundColor: "transparent"
    height: 0
    opacity: 0

  caseLayer__content = new Layer
    superLayer: caseLayersContainer__item
    height: 380
    width: caseLayersContainer__item.width
    image: "img/hands-content.png"
    y: 40

  caseLayersContainer__item.animate
    properties:
      opacity: 1
      height: caseLayer__content.height + 40
    time: 0.4



# add a temporary access case layer when click on the access button
caseBtnContainer__access.onClick ->
  caseLayersContainer__item = new Layer
    superLayer: caseLayersContainer
    width: caseLayersContainer.width
    name: 'caseLayer__access'
    backgroundColor: "transparent"
    height: 0
    opacity: 0

  caseLayer__content = new Layer
    superLayer: caseLayersContainer__item
    height: 541
    width: caseLayersContainer__item.width
    image: "img/access-content.png"
    y: 40

  caseLayersContainer__item.animate
    properties:
      opacity: 1
      height: caseLayer__content.height + 40
    time: 0.4


# add a delivery and removal case layer when click on the delivery button
caseBtnContainer__delivery.onClick ->
  caseLayersContainer__item = new Layer
    superLayer: caseLayersContainer
    width: caseLayersContainer.width
    name: 'caseLayer__delivery'
    backgroundColor: "transparent"
    height: 0
    opacity: 0

  caseLayer__content = new Layer
    superLayer: caseLayersContainer__item
    height: 430
    width: caseLayersContainer__item.width
    image: "img/delivery-content.png"
    y: 40

  caseLayersContainer__item.animate
    properties:
      opacity: 1
      height: caseLayer__content.height + 40
    time: 0.4



for layer in caseBtnContainer.children
  layer.onClick ->
    caseArray = caseLayersContainer.children
    
    for i in [0...caseArray.length]      
      caseArray[i].clip = true

      caseArray[i].states.animationOptions =
        curve: "ease"
        time: 0.4

      caseArray[i].states.add
        collapse:
          height: 40
        full:
          height: caseArray[i].contentFrame().height + 40

      if caseArray[i].children.length == 1
        # create a header bar
        caseLayer__header = new Layer
          superLayer: caseArray[i]
          width: caseArray[i].width - 120
          backgroundColor: "transparent"
          name: "caseLayer__header"
          height: 40

        caseLayer__header.onClick ->
          caseLayer__header.parent.states.next("collapse", "full")


        # create a title layer
        caseLayer__title = new Layer
          superLayer: caseLayer__header
          name: "caseLayer__title"
          height: 40
          width: caseLayer__header.width

        # create remove btn
        removeCaseLayer = new Layer
          superLayer: caseArray[i]
          width: 120
          height: 40;
          backgroundColor: Utils.randomChoice(palette)
          x: Align.right

        removeCaseLayer.onClick (event,layer) ->
          layer.parent.animate
            properties:
              opacity: 0.1
              height: 0
            time: 0.4

          layer.parent.onAnimationEnd ->
            layer.parent.destroy()
            caseArray = caseLayersContainer.children

            if caseLayersContainer.children.length == 0
              caseLayersContainer.height = 0
              caseBtnContainer.y = caseLayersContainer.y + caseLayersContainer.height

            # rearrange cases layer when a case layer is removed
            for i in [0...caseArray.length]
              j = i - 1
              if i == 0
                caseArray[i].y = 0
              else
                caseArray[i].y = caseArray[j].y + caseArray[j].height + 10 
              caseLayersContainer.height = caseLayersContainer.contentFrame().height
              caseBtnContainer.y = caseLayersContainer.y + caseLayersContainer.height + 10
              if mainContainerArray[0].contentFrame().height > 140
                mainContainerArray[0].height = mainContainerArray[0].contentFrame().height + 30

      caseArray[i].height = caseArray[i].contentFrame().height
    

      # rearrange cases layer when a case layer is added
      j = i - 1
      if i == 0
        caseArray[i].y = 0
      else
        caseArray[i].y = caseArray[j].y + caseArray[j].height + 10 
      caseLayersContainer.height = caseLayersContainer.contentFrame().height
      caseBtnContainer.y = caseLayersContainer.y + caseLayersContainer.height + 10
      if mainContainerArray[0].contentFrame().height > 140
        mainContainerArray[0].height = mainContainerArray[0].contentFrame().height + 30


    # name the Hands And Eyes case layers
    caseLayersHandsArray = caseLayersContainer.childrenWithName('caseLayer__hands')
    for i in [0...caseLayersHandsArray.length]
      descLayers = caseLayersHandsArray[i].descendants
      for layer in descLayers
        if layer.name == "caseLayer__title"
          layer.html = "Hands And Eyes"
          if i != 0
            layer.html = layer.html + " (" + (i+1) + ")"

    # name the Temporary Access case layers
    caseLayersAccessArray = caseLayersContainer.childrenWithName('caseLayer__access')
    for i in [0...caseLayersAccessArray.length]
      descLayers = caseLayersAccessArray[i].descendants
      for layer in descLayers
        if layer.name == "caseLayer__title"
          layer.html = "Temporary Access"
          if i != 0
            layer.html = layer.html + " (" + (i+1) + ")"

    # name the Delivery And Removal case layers
    caseLayersDeliveryArray = caseLayersContainer.childrenWithName('caseLayer__delivery')
    for i in [0...caseLayersDeliveryArray.length]
      descLayers = caseLayersDeliveryArray[i].descendants
      for layer in descLayers
        if layer.name == "caseLayer__title"
          layer.html = "Delivery And Removal"
          if i != 0
            layer.html = layer.html + " (" + (i+1) + ")"


    for layer in caseLayersContainer.children
      layer.on "change:height", ->
        # rearrange cases layer when a case layer is removed
        for i in [0...caseArray.length]
          j = i - 1
          if i == 0
            caseArray[i].y = 0
          else
            caseArray[i].y = caseArray[j].y + caseArray[j].height + 10 
          caseLayersContainer.height = caseLayersContainer.contentFrame().height
          caseBtnContainer.y = caseLayersContainer.y + caseLayersContainer.height + 10
          if mainContainerArray[0].contentFrame().height > 140
            mainContainerArray[0].height = mainContainerArray[0].contentFrame().height + 30






# resize update : probably not good for performance
Events.wrap(window).addEventListener "resize", (event) ->
  page.width = Screen.width
  page.height = Screen.height

  for i in [0..numberOfPage-1]
    pageArray[i].width = Screen.width
    pageArray[i].height = Screen.height
    pageArray[i].x = Screen.width * i







# example to use layers defined in an external file
# module = require './modules/test'
# module.mylayerA.props =
#   x: 0
#   y: 100
# module.mylayerA.superLayer = mainContainerArray[1]





# # loop to generate some random bloc
# for i in [0..5]
#   blocItem = new Layer
#     superLayer: pageArray[0]
#     width: 200
#     height: 500
#     backgroundColor: Utils.randomChoice(palette)
#     y: 505 * i + 5

#   blocItem.centerX()