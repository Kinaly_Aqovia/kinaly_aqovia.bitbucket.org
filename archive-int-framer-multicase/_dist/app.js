(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var caseBtnContainer, caseBtnContainer__access, caseBtnContainer__delivery, caseBtnContainer__hands, caseLayersContainer, i, k, l, layer, len, mainContainer, mainContainerArray, numberOfPage, page, pageArray, pageItem, palette, ref, ref1;

palette = ['#f2eeb1', '#e1edce', '#b7d0c1', '#8ab3b3', '#668391', '#3f5270', '#6e4c60', '#9d4651', '#cf3e40', '#d66b5f', '#df967c', '#e8c198'];

page = new PageComponent({
  width: Screen.width,
  height: Screen.height,
  scrollVertical: false,
  backgroundColor: 'white'
});

page.content.draggable.enabled = false;

numberOfPage = 1;

pageArray = [];

mainContainerArray = [];

for (i = k = 0, ref = numberOfPage - 1; 0 <= ref ? k <= ref : k >= ref; i = 0 <= ref ? ++k : --k) {
  pageItem = new Layer({
    parent: page.content,
    width: Screen.width,
    height: Screen.height,
    x: Screen.width * i,
    backgroundColor: Utils.randomChoice(palette),
    name: i
  });
  pageItem.scroll = true;
  pageItem.scrollHorizontal = false;
  pageItem.mouseWheelEnabled = true;
  pageArray.push(pageItem);
  mainContainer = new Layer({
    width: 800,
    height: 200,
    backgroundColor: 'white',
    superLayer: pageArray[i]
  });
  mainContainer.centerY();
  mainContainer.style = {
    width: '800px',
    marginLeft: 'calc((100% - 800px) / 2)'
  };
  mainContainer.scroll = true;
  mainContainer.scrollHorizontal = false;
  mainContainer.mouseWheelEnabled = true;
  mainContainerArray.push(mainContainer);
}

mainContainerArray[0].y = 20;

caseBtnContainer = new Layer({
  superLayer: mainContainerArray[0],
  width: 750,
  height: 40,
  backgroundColor: 'transparent',
  x: 10,
  y: 10
});

caseBtnContainer.centerX();

caseBtnContainer__hands = new Layer({
  superLayer: caseBtnContainer,
  width: 114,
  height: 24,
  image: "img/btn-hands.png"
});

caseBtnContainer__access = new Layer({
  superLayer: caseBtnContainer,
  width: 136,
  height: 24,
  image: "img/btn-access.png",
  x: caseBtnContainer__hands.x + caseBtnContainer__hands.width + 20
});

caseBtnContainer__delivery = new Layer({
  superLayer: caseBtnContainer,
  width: 155,
  height: 24,
  image: "img/btn-delivery.png",
  x: caseBtnContainer__access.x + caseBtnContainer__access.width + 20
});

caseLayersContainer = new Layer({
  superLayer: mainContainerArray[0],
  width: 750,
  backgroundColor: "transparent",
  height: 0,
  x: 10,
  y: 10
});

caseLayersContainer.centerX();

caseBtnContainer__hands.onClick(function() {
  var caseLayer__content, caseLayersContainer__item;
  caseLayersContainer__item = new Layer({
    superLayer: caseLayersContainer,
    width: caseLayersContainer.width,
    name: 'caseLayer__hands',
    backgroundColor: "transparent",
    height: 0,
    opacity: 0
  });
  caseLayer__content = new Layer({
    superLayer: caseLayersContainer__item,
    height: 380,
    width: caseLayersContainer__item.width,
    image: "img/hands-content.png",
    y: 40
  });
  return caseLayersContainer__item.animate({
    properties: {
      opacity: 1,
      height: caseLayer__content.height + 40
    },
    time: 0.4
  });
});

caseBtnContainer__access.onClick(function() {
  var caseLayer__content, caseLayersContainer__item;
  caseLayersContainer__item = new Layer({
    superLayer: caseLayersContainer,
    width: caseLayersContainer.width,
    name: 'caseLayer__access',
    backgroundColor: "transparent",
    height: 0,
    opacity: 0
  });
  caseLayer__content = new Layer({
    superLayer: caseLayersContainer__item,
    height: 541,
    width: caseLayersContainer__item.width,
    image: "img/access-content.png",
    y: 40
  });
  return caseLayersContainer__item.animate({
    properties: {
      opacity: 1,
      height: caseLayer__content.height + 40
    },
    time: 0.4
  });
});

caseBtnContainer__delivery.onClick(function() {
  var caseLayer__content, caseLayersContainer__item;
  caseLayersContainer__item = new Layer({
    superLayer: caseLayersContainer,
    width: caseLayersContainer.width,
    name: 'caseLayer__delivery',
    backgroundColor: "transparent",
    height: 0,
    opacity: 0
  });
  caseLayer__content = new Layer({
    superLayer: caseLayersContainer__item,
    height: 430,
    width: caseLayersContainer__item.width,
    image: "img/delivery-content.png",
    y: 40
  });
  return caseLayersContainer__item.animate({
    properties: {
      opacity: 1,
      height: caseLayer__content.height + 40
    },
    time: 0.4
  });
});

ref1 = caseBtnContainer.children;
for (l = 0, len = ref1.length; l < len; l++) {
  layer = ref1[l];
  layer.onClick(function() {
    var caseArray, caseLayer__header, caseLayer__title, caseLayersAccessArray, caseLayersDeliveryArray, caseLayersHandsArray, descLayers, j, len1, len2, len3, len4, m, n, o, p, q, r, ref2, ref3, ref4, ref5, ref6, removeCaseLayer, results, s, t;
    caseArray = caseLayersContainer.children;
    for (i = m = 0, ref2 = caseArray.length; 0 <= ref2 ? m < ref2 : m > ref2; i = 0 <= ref2 ? ++m : --m) {
      caseArray[i].clip = true;
      caseArray[i].states.animationOptions = {
        curve: "ease",
        time: 0.4
      };
      caseArray[i].states.add({
        collapse: {
          height: 40
        },
        full: {
          height: caseArray[i].contentFrame().height + 40
        }
      });
      if (caseArray[i].children.length === 1) {
        caseLayer__header = new Layer({
          superLayer: caseArray[i],
          width: caseArray[i].width - 120,
          backgroundColor: "transparent",
          name: "caseLayer__header",
          height: 40
        });
        caseLayer__header.onClick(function() {
          return caseLayer__header.parent.states.next("collapse", "full");
        });
        caseLayer__title = new Layer({
          superLayer: caseLayer__header,
          name: "caseLayer__title",
          height: 40,
          width: caseLayer__header.width
        });
        removeCaseLayer = new Layer({
          superLayer: caseArray[i],
          width: 120,
          height: 40,
          backgroundColor: Utils.randomChoice(palette),
          x: Align.right
        });
        removeCaseLayer.onClick(function(event, layer) {
          layer.parent.animate({
            properties: {
              opacity: 0.1,
              height: 0
            },
            time: 0.4
          });
          return layer.parent.onAnimationEnd(function() {
            var j, n, ref3, results;
            layer.parent.destroy();
            caseArray = caseLayersContainer.children;
            if (caseLayersContainer.children.length === 0) {
              caseLayersContainer.height = 0;
              caseBtnContainer.y = caseLayersContainer.y + caseLayersContainer.height;
            }
            results = [];
            for (i = n = 0, ref3 = caseArray.length; 0 <= ref3 ? n < ref3 : n > ref3; i = 0 <= ref3 ? ++n : --n) {
              j = i - 1;
              if (i === 0) {
                caseArray[i].y = 0;
              } else {
                caseArray[i].y = caseArray[j].y + caseArray[j].height + 10;
              }
              caseLayersContainer.height = caseLayersContainer.contentFrame().height;
              caseBtnContainer.y = caseLayersContainer.y + caseLayersContainer.height + 10;
              if (mainContainerArray[0].contentFrame().height > 140) {
                results.push(mainContainerArray[0].height = mainContainerArray[0].contentFrame().height + 30);
              } else {
                results.push(void 0);
              }
            }
            return results;
          });
        });
      }
      caseArray[i].height = caseArray[i].contentFrame().height;
      j = i - 1;
      if (i === 0) {
        caseArray[i].y = 0;
      } else {
        caseArray[i].y = caseArray[j].y + caseArray[j].height + 10;
      }
      caseLayersContainer.height = caseLayersContainer.contentFrame().height;
      caseBtnContainer.y = caseLayersContainer.y + caseLayersContainer.height + 10;
      if (mainContainerArray[0].contentFrame().height > 140) {
        mainContainerArray[0].height = mainContainerArray[0].contentFrame().height + 30;
      }
    }
    caseLayersHandsArray = caseLayersContainer.childrenWithName('caseLayer__hands');
    for (i = n = 0, ref3 = caseLayersHandsArray.length; 0 <= ref3 ? n < ref3 : n > ref3; i = 0 <= ref3 ? ++n : --n) {
      descLayers = caseLayersHandsArray[i].descendants;
      for (o = 0, len1 = descLayers.length; o < len1; o++) {
        layer = descLayers[o];
        if (layer.name === "caseLayer__title") {
          layer.html = "Hands And Eyes";
          if (i !== 0) {
            layer.html = layer.html + " (" + (i + 1) + ")";
          }
        }
      }
    }
    caseLayersAccessArray = caseLayersContainer.childrenWithName('caseLayer__access');
    for (i = p = 0, ref4 = caseLayersAccessArray.length; 0 <= ref4 ? p < ref4 : p > ref4; i = 0 <= ref4 ? ++p : --p) {
      descLayers = caseLayersAccessArray[i].descendants;
      for (q = 0, len2 = descLayers.length; q < len2; q++) {
        layer = descLayers[q];
        if (layer.name === "caseLayer__title") {
          layer.html = "Temporary Access";
          if (i !== 0) {
            layer.html = layer.html + " (" + (i + 1) + ")";
          }
        }
      }
    }
    caseLayersDeliveryArray = caseLayersContainer.childrenWithName('caseLayer__delivery');
    for (i = r = 0, ref5 = caseLayersDeliveryArray.length; 0 <= ref5 ? r < ref5 : r > ref5; i = 0 <= ref5 ? ++r : --r) {
      descLayers = caseLayersDeliveryArray[i].descendants;
      for (s = 0, len3 = descLayers.length; s < len3; s++) {
        layer = descLayers[s];
        if (layer.name === "caseLayer__title") {
          layer.html = "Delivery And Removal";
          if (i !== 0) {
            layer.html = layer.html + " (" + (i + 1) + ")";
          }
        }
      }
    }
    ref6 = caseLayersContainer.children;
    results = [];
    for (t = 0, len4 = ref6.length; t < len4; t++) {
      layer = ref6[t];
      results.push(layer.on("change:height", function() {
        var ref7, results1, u;
        results1 = [];
        for (i = u = 0, ref7 = caseArray.length; 0 <= ref7 ? u < ref7 : u > ref7; i = 0 <= ref7 ? ++u : --u) {
          j = i - 1;
          if (i === 0) {
            caseArray[i].y = 0;
          } else {
            caseArray[i].y = caseArray[j].y + caseArray[j].height + 10;
          }
          caseLayersContainer.height = caseLayersContainer.contentFrame().height;
          caseBtnContainer.y = caseLayersContainer.y + caseLayersContainer.height + 10;
          if (mainContainerArray[0].contentFrame().height > 140) {
            results1.push(mainContainerArray[0].height = mainContainerArray[0].contentFrame().height + 30);
          } else {
            results1.push(void 0);
          }
        }
        return results1;
      }));
    }
    return results;
  });
}

Events.wrap(window).addEventListener("resize", function(event) {
  var m, ref2, results;
  page.width = Screen.width;
  page.height = Screen.height;
  results = [];
  for (i = m = 0, ref2 = numberOfPage - 1; 0 <= ref2 ? m <= ref2 : m >= ref2; i = 0 <= ref2 ? ++m : --m) {
    pageArray[i].width = Screen.width;
    pageArray[i].height = Screen.height;
    results.push(pageArray[i].x = Screen.width * i);
  }
  return results;
});


},{}]},{},[1]);
