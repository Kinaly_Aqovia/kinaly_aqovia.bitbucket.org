// from Ditching jQuery

// https://gomakethings.com/ditching-jquery/#manipulate-height
/**
 * Get the height of an element
 * @param  {Node}   elem The element
 * @return {Number}      The height
 */
var getHeight = function(elem) {
  return Math.max(elem.scrollHeight, elem.offsetHeight, elem.clientHeight);
};



// https://gomakethings.com/ditching-jquery/#climb-up-the-dom
/**
 * Get the closest matching element up the DOM tree.
 * @private
 * @param  {Element} elem     Starting element
 * @param  {String}  selector Selector to match against
 * @return {Boolean|Element}  Returns null if not match found
 */
var getClosest = function(elem, selector) {
  // Element.matches() polyfill
  if (!Element.prototype.matches) {
    Element.prototype.matches =
      Element.prototype.matchesSelector ||
      Element.prototype.mozMatchesSelector ||
      Element.prototype.msMatchesSelector ||
      Element.prototype.oMatchesSelector ||
      Element.prototype.webkitMatchesSelector ||
      function(s) {
        var matches = (this.document || this.ownerDocument).querySelectorAll(s),
          i = matches.length;
        while (--i >= 0 && matches.item(i) !== this) {}
        return i > -1;
      };
  }

  // Get closest match
  for (; elem && elem !== document; elem = elem.parentNode) {
    if (elem.matches(selector)) return elem;
  }

  return null;
};



// toggle filters

var allFiltersNames = document.querySelectorAll("input[name='inventoryFiltersChoice']");
var areFiltersActive = false;

for (var i = 0, all = allFiltersNames.length; i < all; i++) {
  allFiltersNames[i].addEventListener(
    "click",
    function(e) {
    	var elem = this;
    	var elemIdString = this.id;
    	var elemType = elemIdString.substring(elemIdString.lastIndexOf("_")+1,elemIdString.length);
    	

    	// find the element whose ID ends like elemType
    	var targetControls = document.getElementById("inventoryFiltersControls__"+elemType);
    	
    	// get the height of the content to show
    	var targetControlsInnerElem = document.querySelector("#inventoryFiltersControls__"+ elemType + "> .inventory-filters-controls__content");
    	var targetControlsHeight = getHeight(targetControlsInnerElem);

    	// toggle class when input is checked
    	if (elem.checked == true) {
      	targetControls.classList.add("-is-open");
    	} else {
    		targetControls.classList.remove("-is-open");

    		// ideally we would also default back to "all" option but not sure how to generate the event to trigger the filters
    	}

    	// change the height of targetControls
    	if (targetControls.classList.contains("-is-open")) {
    	  targetControls.style.height = targetControlsHeight + "px";
    	} else {
    	  targetControls.style.height = "0";
    	}  

    	// update value of areFiltersActive to know if a filter is used or not
    	var activeFilters = document.querySelectorAll(".inventory-filters-controls__item.-is-open");

    	if (activeFilters.length > 0) {
    		areFiltersActive = true;
    	} else {
    		areFiltersActive = false;
    	}
    },
    false
  );
}




// pseudo log on screen
var allLogBoxes = document.querySelectorAll(".proto-only-log-box");




var inventorySearch = document.querySelector("#inventorySearch");
var allPortsCharac = document.querySelectorAll(".ports-inventory__dynamic-criteria");

var allPorts = document.querySelectorAll(".ports-inventory__item");
var searchedPorts = allPorts;


var inventoryHeadingInitial = document.querySelector(".initial-inventory-heading");
var inventoryHeadingUpdated = document.querySelector(".updated-inventory-heading");


var searchValue = "";

// add keyup event to the inventory search input
inventorySearch.addEventListener("keyup", function(e) {
	
	// capture the value of the search input
	searchValue = inventorySearch.value.toLowerCase();

	// test the value of the inventory search input against each ports characteristics
	for (var i = 0, all = allPortsCharac.length; i < all; i++) {
		// find closest port item where the port charac are tested against
		var mainParent = getClosest(allPortsCharac[i], ".ports-inventory__item");
		
		// value to test search input against
		var portCriteriaString = allPortsCharac[i].textContent.toLowerCase();

		// find all criteria for a port to know which one to display later
		var portCriteriaCollection = allPortsCharac[i].querySelectorAll(".ports-inventory__criteria");


		// if search is less than 3 characters long
		if (searchValue.length < 3) {
			// hide secondary info in port summary
			for (var j = 0, crit = portCriteriaCollection.length; j < crit; j++) {
				portCriteriaCollection[j].classList.remove("-is-shown");
			}

			// if no active filters, display all ports
			if (areFiltersActive == false) {
				for (var j = allPorts.length - 1; j >= 0; j--) {
					allPorts[j].classList.remove("-is-hidden","-by-search");
				}

			// if active filters, only display filtered ports by removing the -by-search class
			} else {
				for (var j = allPorts.length - 1; j >= 0; j--) {
					if (allPorts[j].classList.contains("-by-filters") == true && allPorts[j].classList.contains("-by-search") == true ) {
						allPorts[j].classList.remove("-by-search");
					
					} else if (allPorts[j].classList.contains("-is-hidden") == true && allPorts[j].classList.contains("-by-search") == true && allPorts[j].classList.contains("-by-filters") == false) {
						allPorts[j].classList.remove("-is-hidden","-by-search");
					}
				}
			}

		// if search is 3 and more characters long with a match show item
		} else if (searchValue.length > 2 && portCriteriaString.indexOf(searchValue) !== -1) {
			// find matching criteria and highlight query in results
			for (var j = 0, crit = portCriteriaCollection.length; j < crit; j++) {
				var criteriaValue = portCriteriaCollection[j].textContent.toLowerCase();

				var criteriaHtml = portCriteriaCollection[j].innerHTML;
				var criteriaHtmlLowercase = criteriaHtml.toLowerCase();

				// remove existing highlight
				if (criteriaHtml.length != criteriaValue.length) {
					var insideSpan = portCriteriaCollection[j].querySelector(".search-highlight");
					var insideSpanHtml = insideSpan.innerHTML;

					var noHighlightCriteria = criteriaHtml.replace("<span class=\"search-highlight\">"+ insideSpanHtml +"</span>", insideSpanHtml);

					portCriteriaCollection[j].innerHTML = noHighlightCriteria;

					// reupdate values
					criteriaHtml = portCriteriaCollection[j].innerHTML;
					criteriaHtmlLowercase = criteriaHtml.toLowerCase();
				}

				if (criteriaValue.indexOf(searchValue) !== -1) {
					// show matching criteria in port summary
					portCriteriaCollection[j].classList.add("-is-shown");
					
					// highlight matching search
					var highlightStart = criteriaHtmlLowercase.indexOf(searchValue);
					var extractedQuery = criteriaHtml.substr(highlightStart,searchValue.length);

					var highlightCriteria = criteriaHtml.replace(extractedQuery,"<span class=\"search-highlight\">" + extractedQuery + "</span>");
					portCriteriaCollection[j].innerHTML = highlightCriteria;
				}


			}

			// show ports that match the search and the filters
			if (mainParent.classList.contains("-by-search") == true && mainParent.classList.contains("-by-filters") == false ) {
				mainParent.classList.remove("-is-hidden","-by-search");
			
			} else if (mainParent.classList.contains("-by-search") == true && mainParent.classList.contains("-by-filters") == true) {
				mainParent.classList.remove("-by-search");
			}

			// show ports inventory heading with all ports				
			inventoryHeadingInitial.style.display = "none";
			inventoryHeadingUpdated.style.display = "block";
		
		// if search is more than 3 characters and there is no match hide port item
		} else {
			mainParent.classList.add("-is-hidden","-by-search");
		}
	}

	// update ports inventory heading
	var visiblePorts = document.querySelectorAll(".ports-inventory__item:not(.-is-hidden)").length;
	inventoryHeadingUpdated.textContent = visiblePorts + " ports found";

	if (visiblePorts == allPorts.length) {
		inventoryHeadingInitial.style.display = "block";
		inventoryHeadingUpdated.style.display = "none";
	} else {
		inventoryHeadingInitial.style.display = "none";
		inventoryHeadingUpdated.style.display = "block";
	}

	// update searchedPorts array to use in filters, we also capture the ports that are hidden because of a filter
	searchedPorts = document.querySelectorAll(".ports-inventory__item:not(.-by-search)");

}, false);



var allFilters = document.querySelectorAll("[name='inventoryFilterMediaType'],[name='inventoryFilterStatus'],[name='inventoryFilterCircuitType']");

for (var i = 0, allF = allFilters.length; i < allF; i++) { 
	allFilters[i].addEventListener("change", function(e) {

		var checkedFilters = document.querySelectorAll("[name='inventoryFilterMediaType']:checked,[name='inventoryFilterStatus']:checked,[name='inventoryFilterCircuitType']:checked");

		for (var j = searchedPorts.length - 1; j >= 0; j--) {			
			var circuitTypeElem = searchedPorts[j].querySelector(".ports-inventory__criteria.-circuitType");
			var circuitTypeValue = circuitTypeElem.textContent.toLowerCase();


			var mediaTypeElem = searchedPorts[j].querySelector(".ports-inventory__criteria.-mediaType");
			var mediaTypeValue = mediaTypeElem.textContent.toLowerCase();

			var statusElem = searchedPorts[j].querySelector(".ports-inventory__criteria.-status");
			var statusValue = statusElem.textContent.toLowerCase();

			// serie of variables for each filter (apart from the all option)
			var circuitTypePrecable = false;
			var circuitTypeCrossConnect = false;
			var circuitTypeCloudAccess = false;
			var mediaTypeCoax = false;
			var mediaTypeMmf = false;
			var mediaTypeSmf = false;
			var mediaTypeTwistedPair = false;
			var mediaTypeUnknown = false;
			var statusActive = false;
			var statusReserved = false;
			var statusDefective = false;
			
			for (var k = checkedFilters.length - 1; k >= 0; k--) {
				var checkedId = checkedFilters[k].id;

				// circuit type filters
				if (checkedId == "inventoryFilterCircuitType__Precabled" && circuitTypeValue.indexOf("precable") === -1) {
					circuitTypePrecable = true;
				}

				if (checkedId == "inventoryFilterCircuitType__CrossConnect" && circuitTypeValue.indexOf("cross connect") === -1) {
					circuitTypeCrossConnect = true;
				}

				if (checkedId == "inventoryFilterCircuitType__CloudAccess" && circuitTypeValue.indexOf("cloud access") === -1) {
					circuitTypeCloudAccess = true;
				}

				// media type
				if (checkedId == "inventoryFilterMediaType__Coax" && mediaTypeValue.indexOf("coax") === -1) {
					mediaTypeCoax = true;
				}

				if (checkedId == "inventoryFilterMediaType__MultiModeFibre" && mediaTypeValue.indexOf("multi-mode fibre") === -1) {
					mediaTypeMmf = true;
				}

				if (checkedId == "inventoryFilterMediaType__SMF" && mediaTypeValue.indexOf("smf") === -1) {
					mediaTypeSmf = true;
				}

				if (checkedId == "inventoryFilterMediaType__TwistedPair" && mediaTypeValue.indexOf("twisted pair") === -1) {
					mediaTypeTwistedPair = true;
				}

				if (checkedId == "inventoryFilterMediaType__unknown" && mediaTypeValue.length !== 14) {
					mediaTypeUnknown = true;
				}

				// status filter
				if (checkedId == "inventoryFilterStatus__Active" && statusValue.indexOf("active") === -1) {
					statusActive = true;
				}

				if (checkedId == "inventoryFilterStatus__Reserved" && statusValue.indexOf("reserved") === -1) {
					statusReserved = true;
				}

				if (checkedId == "inventoryFilterStatus__Defective" && statusValue.indexOf("defective") === -1) {
					statusDefective = true;
				}

				if (checkedId == "inventoryFilterStatus__Precabled" && statusValue.indexOf("precabled") === -1) {
					statusDefective = true;
				}

				if (circuitTypePrecable == true || 
					circuitTypeCrossConnect == true || 
					circuitTypeCloudAccess == true ||
					mediaTypeCoax == true ||
					mediaTypeMmf == true ||
					mediaTypeSmf == true ||
					mediaTypeTwistedPair == true ||
					mediaTypeUnknown == true ||
					statusActive == true ||
					statusReserved == true ||
					statusDefective == true
					) {

					searchedPorts[j].classList.add("-is-hidden","-by-filters");
				
				} else if (searchedPorts[j].classList.contains("-by-search") == false) {
					
					searchedPorts[j].classList.remove("-is-hidden","-by-filters");
				
				} else if (searchedPorts[j].classList.contains("-by-search") == true) {
					
					searchedPorts[j].classList.remove("-by-filters");
				}
			}
		}

		// update ports inventory heading
		var visiblePorts = document.querySelectorAll(".ports-inventory__item:not(.-is-hidden)").length;
		inventoryHeadingUpdated.textContent = visiblePorts + " ports found";



	console.log("in filter " + visiblePorts);


	


		if (visiblePorts == allPorts.length) {
			inventoryHeadingInitial.style.display = "block";
			inventoryHeadingUpdated.style.display = "none";
		} else {
			inventoryHeadingInitial.style.display = "none";
			inventoryHeadingUpdated.style.display = "block";
		}
	}, false);
}

