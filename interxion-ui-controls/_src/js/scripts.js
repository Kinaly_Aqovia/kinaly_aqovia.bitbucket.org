// from Ditching jQuery

// https://gomakethings.com/ditching-jquery/#manipulate-height
/**
 * Get the height of an element
 * @param  {Node}   elem The element
 * @return {Number}      The height
 */
var getHeight = function(elem) {
  return Math.max(elem.scrollHeight, elem.offsetHeight, elem.clientHeight);
};

// https://gomakethings.com/ditching-jquery/#climb-up-the-dom
/**
 * Get the closest matching element up the DOM tree.
 * @private
 * @param  {Element} elem     Starting element
 * @param  {String}  selector Selector to match against
 * @return {Boolean|Element}  Returns null if not match found
 */
var getClosest = function(elem, selector) {
  // Element.matches() polyfill
  if (!Element.prototype.matches) {
    Element.prototype.matches =
      Element.prototype.matchesSelector ||
      Element.prototype.mozMatchesSelector ||
      Element.prototype.msMatchesSelector ||
      Element.prototype.oMatchesSelector ||
      Element.prototype.webkitMatchesSelector ||
      function(s) {
        var matches = (this.document || this.ownerDocument).querySelectorAll(s),
          i = matches.length;
        while (--i >= 0 && matches.item(i) !== this) {}
        return i > -1;
      };
  }

  // Get closest match
  for (; elem && elem !== document; elem = elem.parentNode) {
    if (elem.matches(selector)) return elem;
  }

  return null;
};



// custom

// fetch all items that are collapsible
var allCollapsibleItems = document.querySelectorAll(".collapsible-item");

for (var i = 0, len = allCollapsibleItems.length; i < len; i++) {
  // find all triggers
  var allItemTriggers = allCollapsibleItems[i].querySelectorAll(
    ".collapsible-item__trigger"
  );

  for (var j = 0, all = allItemTriggers.length; j < all; j++) {
    // add a listener to the trigger
    allItemTriggers[j].addEventListener(
      "click",
      function(e) {
        // find the item the trigger is linked to
        var mainParent = getClosest(this, ".collapsible-item");

        // find the outer div that will vary in height
        var itemShell = mainParent.querySelector(
          ".collapsible-item__expanding-shell"
        );

        // find the height of the content
        var itemContent = mainParent.querySelector(
          ".collapsible-item__hideable-content"
        );
        var contentHeight = getHeight(itemContent);

        // toggle a class for open state
        mainParent.classList.toggle("-is-open");

        // change the height of the outer div
        if (mainParent.classList.contains("-is-open")) {
          itemShell.style.height = contentHeight + "px";
        } else {
          itemShell.style.height = "0";
        }
      },
      false
    );
  }
}
