(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var TextLayer, aboveCases, belowCases, btn, bundleDetails, bundleDetails__ref, bundleDetails__summary, caseBtns, caseBtns__item_access, caseBtns__item_delivery, caseBtns__item_hands, caseBtns__title, cases, i, inputBase, inputBgColor, k, l, len, mainContainer, mainContainerArray, mainContainerInitialHeight, myFunction, numberOfPage, page, pageArray, pageItem, palette, ref, ref1, ref2, saveMultiArea, saveMultiBtn, structure, updateHeight, ƒ, ƒƒ;

TextLayer = require('./modules/TextLayer').TextLayer;

ref = require('./modules/findModule'), ƒ = ref.ƒ, ƒƒ = ref.ƒƒ;

inputBgColor = "transparent";

inputBase = {};

inputBase.width = 292;

inputBase.height = 21;

inputBase.backgroundColor = inputBgColor;

inputBase.text = "";

inputBase.contentEditable = true;

inputBase.lineHeight = "21px";

inputBase.paddingLeft = 5;

inputBase.fontSize = 14;

inputBase.color = "#000";

palette = ['#f2eeb1', '#e1edce', '#b7d0c1', '#8ab3b3', '#668391', '#3f5270', '#6e4c60', '#9d4651', '#cf3e40', '#d66b5f', '#df967c', '#e8c198'];

page = new PageComponent({
  width: Screen.width,
  height: Screen.height,
  scrollVertical: false,
  backgroundColor: 'white'
});

page.content.draggable.enabled = false;

numberOfPage = 1;

pageArray = [];

mainContainerArray = [];

mainContainerInitialHeight = 800;

for (i = k = 0, ref1 = numberOfPage - 1; 0 <= ref1 ? k <= ref1 : k >= ref1; i = 0 <= ref1 ? ++k : --k) {
  pageItem = new Layer({
    parent: page.content,
    width: Screen.width,
    height: Screen.height,
    x: Screen.width * i,
    backgroundColor: "#fff",
    name: i
  });
  pageItem.scroll = true;
  pageItem.scrollHorizontal = false;
  pageItem.mouseWheelEnabled = true;
  pageArray.push(pageItem);
  mainContainer = new Layer({
    width: 985,
    height: mainContainerInitialHeight,
    backgroundColor: 'white',
    superLayer: pageArray[i],
    y: 0
  });
  mainContainer.style = {
    width: '985px',
    marginLeft: 'calc((100% - 985px) / 2)'
  };
  mainContainer.scroll = true;
  mainContainer.scrollHorizontal = false;
  mainContainer.mouseWheelEnabled = true;
  mainContainerArray.push(mainContainer);
}

structure = new Layer({
  superLayer: mainContainerArray[0],
  width: mainContainerArray[0].width,
  height: 1178
});

structure.style = {
  "background": "url('img/structure.png') 50% 0 no-repeat"
};

updateHeight = function(layer) {
  var layerHeight;
  if (layer === mainContainerArray[0]) {
    layerHeight = layer.contentFrame().height + 60;
  } else {
    layerHeight = layer.contentFrame().height;
  }
  return layerHeight;
};

myFunction = function(input) {
  var output;
  output = input * 2;
  return output;
};

aboveCases = new Layer({
  superLayer: mainContainerArray[0],
  width: 756,
  height: 589,
  image: "img/multi-case-header.png",
  x: 211,
  y: 167
});

bundleDetails = new Layer({
  superLayer: aboveCases,
  backgroundColor: "transparent",
  width: aboveCases.width
});

bundleDetails__ref = new TextLayer({
  width: inputBase.width,
  height: inputBase.height,
  backgroundColor: inputBase.backgroundColor,
  text: inputBase.text,
  contentEditable: inputBase.contentEditable,
  lineHeight: inputBase.lineHeight,
  paddingLeft: inputBase.paddingLeft,
  fontSize: inputBase.fontSize,
  color: inputBase.color,
  superLayer: bundleDetails,
  x: 23,
  y: 269
});

bundleDetails__summary = new TextLayer({
  width: inputBase.width,
  height: inputBase.height,
  backgroundColor: inputBase.backgroundColor,
  text: inputBase.text,
  contentEditable: inputBase.contentEditable,
  lineHeight: inputBase.lineHeight,
  paddingLeft: inputBase.paddingLeft,
  fontSize: inputBase.fontSize,
  color: inputBase.color,
  superLayer: bundleDetails,
  superLayer: bundleDetails,
  x: bundleDetails__ref.x,
  y: bundleDetails__ref.y + bundleDetails__ref.height + 39
});

cases = new Layer({
  superLayer: mainContainerArray[0],
  width: 750,
  height: 0,
  backgroundColor: "transparent",
  name: "casesLayer",
  y: aboveCases.height + aboveCases.y,
  x: 213
});

belowCases = new Layer({
  superLayer: mainContainerArray[0],
  width: 750,
  backgroundColor: "transparent",
  y: cases.height + cases.y,
  x: 213
});

caseBtns = new Layer({
  superLayer: belowCases,
  width: 750,
  height: 78,
  y: 20,
  backgroundColor: "#e6f1f4",
  borderColor: "#ccddec",
  borderWidth: 1,
  borderRadius: 5
});

saveMultiArea = new Layer({
  superLayer: belowCases,
  height: 200,
  backgroundColor: "transparent",
  width: belowCases.width,
  y: caseBtns.height + caseBtns.y + 20
});

saveMultiBtn = new TextLayer({
  superLayer: saveMultiArea,
  text: "save multi-case",
  image: "img/btn-fat-empty.png",
  width: 327,
  height: 40,
  lineHeight: "40px",
  color: "#fff",
  textAlign: "center",
  x: Align.right,
  opacity: 0.4
});

saveMultiBtn.states.add({
  enabled: {
    opacity: 1
  }
});

belowCases.height = updateHeight(belowCases);

caseBtns__title = new TextLayer({
  superLayer: caseBtns,
  text: "Open a case",
  height: 20,
  color: "#000",
  fontSize: 18,
  x: 20,
  y: 5
});

caseBtns__item_hands = new Layer({
  superLayer: caseBtns,
  image: "img/btn-hands.png",
  width: 114,
  height: 24,
  x: 20,
  y: 34,
  name: "caseBtn__hands"
});

caseBtns__item_access = new Layer({
  superLayer: caseBtns,
  image: "img/btn-access.png",
  width: 136,
  height: 24,
  x: caseBtns__item_hands.x + caseBtns__item_hands.width + 20,
  y: 34,
  name: "caseBtn__access"
});

caseBtns__item_delivery = new Layer({
  superLayer: caseBtns,
  image: "img/btn-delivery.png",
  width: 155,
  height: 24,
  x: caseBtns__item_access.x + caseBtns__item_access.width + 20,
  y: 34,
  name: "caseBtn__delivery"
});

ref2 = caseBtns.children;
for (l = 0, len = ref2.length; l < len; l++) {
  btn = ref2[l];
  btn.onClick(function(event, btn) {
    var caret, casesArray, casesItem, cases__caret, cases__content, cases__header, cases__icon, cases__item, cases__remove, cases__removeBtnsSet, cases__removeCancel, cases__removeConfirm, cases__saveBtn, cases__title, deliveryCase__requestType, deliveryCase__trackingNumber, handCase__phone, handCase__requestType, layer, len1, len2, len3, m, n, o, ref3, ref4, results;
    ref3 = cases.children;
    for (m = 0, len1 = ref3.length; m < len1; m++) {
      layer = ref3[m];
      layer.states["switch"]("close");
      ref4 = layer.ƒƒ("caseCaret");
      for (n = 0, len2 = ref4.length; n < len2; n++) {
        caret = ref4[n];
        caret.states["switch"]("right");
      }
    }
    cases__item = new Layer({
      superLayer: cases,
      width: cases.width,
      opacity: 0,
      name: "caseItem",
      backgroundColor: "transparent",
      borderRadius: 5
    });
    cases__item.height = 0;
    cases__item.clip = true;
    cases__header = new Layer({
      superLayer: cases__item,
      height: 40,
      y: 20,
      width: cases__item.width,
      image: "img/header-bg.png"
    });
    cases__caret = new Layer({
      superLayer: cases__header,
      width: 5,
      height: 10,
      image: "img/icon-caret-right.png",
      x: 13,
      name: "caseCaret",
      rotation: 90
    });
    cases__caret.centerY();
    cases__caret.states.add({
      right: {
        rotation: 0
      },
      down: {
        rotation: 90
      }
    });
    cases__caret.states.animationOptions = {
      time: 0.3
    };
    cases__icon = new Layer({
      superLayer: cases__header,
      width: 20,
      height: 20,
      x: 25
    });
    cases__icon.centerY();
    cases__title = new TextLayer({
      superLayer: cases__header,
      text: "Case Title Here",
      color: "#000",
      fontSize: 18,
      autoSize: true,
      height: 20,
      y: 8,
      x: 48
    });
    cases__remove = new Layer({
      superLayer: cases__item,
      width: 120,
      height: 40,
      image: "img/remove.png",
      x: Align.right,
      y: 20,
      name: "remove area"
    });
    cases__remove.clip = true;
    cases__remove.states.add({
      extended: {
        width: 240,
        x: cases__remove.x - 120
      }
    });
    cases__remove.states.animationOptions = {
      curve: "ease-in-out",
      time: 0.2
    };
    cases__removeBtnsSet = new Layer({
      superLayer: cases__remove,
      width: 2 * cases__remove.width,
      backgroundColor: "transparent",
      x: cases__remove.width
    });
    cases__removeBtnsSet.states.add({
      show: {
        x: 0
      }
    });
    cases__removeBtnsSet.states.animationOptions = {
      curve: "ease-in-out",
      time: 0.2
    };
    cases__removeConfirm = new Layer({
      superLayer: cases__removeBtnsSet,
      width: cases__remove.width,
      height: cases__remove.height,
      image: "img/remove-confirm.png",
      x: 0
    });
    cases__remove.onClick(function(event, layer) {
      var len3, o, ref5, results;
      layer.states.next();
      ref5 = layer.children;
      results = [];
      for (o = 0, len3 = ref5.length; o < len3; o++) {
        btn = ref5[o];
        results.push(btn.states.next());
      }
      return results;
    });
    cases__removeConfirm.onClick(function(event, layer) {
      layer.parent.parent.parent.animate({
        properties: {
          height: 0,
          opacity: 0
        }
      });
      return layer.parent.parent.parent.on(Events.AnimationEnd, function(animation, layer) {
        return layer.destroy();
      });
    });
    cases__removeCancel = new Layer({
      superLayer: cases__removeBtnsSet,
      width: cases__remove.width,
      height: cases__remove.height,
      image: "img/remove-cancel.png",
      x: cases__remove.width
    });
    cases__content = new Layer({
      superLayer: cases__item,
      width: cases__item.width,
      backgroundColor: "transparent",
      y: 60
    });
    cases__saveBtn = new Layer({
      superLayer: cases__content,
      width: 327,
      height: 40,
      image: "img/btn-inside-case.png",
      x: 403
    });
    cases__saveBtn.onClick(function(event, layer) {
      var icon, len3, o, ref5, results;
      layer.parent.parent.states["switch"]("close");
      ref5 = layer.parent.parent.ƒƒ("caseCaret");
      results = [];
      for (o = 0, len3 = ref5.length; o < len3; o++) {
        icon = ref5[o];
        results.push(icon.states["switch"]("right"));
      }
      return results;
    });
    if (btn.name === "caseBtn__hands") {
      cases__content.height = 558;
      cases__content.image = "img/hands-content.png";
      cases__content.name = "caseHandsContent";
      cases__saveBtn.y = 472;
      cases__icon.image = "img/icon-eye.png";
      cases__title.text = "Hands And Eyes";
      handCase__phone = new TextLayer({
        width: 278,
        height: inputBase.height,
        backgroundColor: inputBase.backgroundColor,
        text: inputBase.text,
        contentEditable: inputBase.contentEditable,
        lineHeight: inputBase.lineHeight,
        paddingLeft: inputBase.paddingLeft,
        fontSize: inputBase.fontSize,
        color: inputBase.color,
        superLayer: cases__item,
        name: "handCase__phone",
        x: 40,
        y: 114
      });
      handCase__requestType = new TextLayer({
        width: 278,
        height: inputBase.height,
        backgroundColor: inputBase.backgroundColor,
        text: inputBase.text,
        contentEditable: inputBase.contentEditable,
        lineHeight: inputBase.lineHeight,
        paddingLeft: inputBase.paddingLeft,
        fontSize: inputBase.fontSize,
        color: inputBase.color,
        superLayer: cases__item,
        name: "handCase__requestType",
        x: handCase__phone.x + handCase__phone.width + 82,
        y: 114
      });
    } else if (btn.name === "caseBtn__access") {
      cases__content.height = 718;
      cases__content.image = "img/access-content.png";
      cases__content.name = "caseAccessContent";
      cases__saveBtn.y = 635;
      cases__icon.image = "img/icon-access.png";
      cases__title.text = "Temporary Access";
    } else if (btn.name === "caseBtn__delivery") {
      cases__content.height = 609;
      cases__content.image = "img/delivery-content.png";
      cases__content.name = "caseDeliveryContent";
      cases__saveBtn.y = 524;
      cases__icon.image = "img/icon-delivery.png";
      cases__title.text = "Delivery And Removal";
      deliveryCase__requestType = new TextLayer({
        width: 278,
        height: inputBase.height,
        backgroundColor: inputBase.backgroundColor,
        text: inputBase.text,
        contentEditable: inputBase.contentEditable,
        lineHeight: inputBase.lineHeight,
        paddingLeft: inputBase.paddingLeft,
        fontSize: inputBase.fontSize,
        color: inputBase.color,
        superLayer: cases__item,
        name: "deliveryCase__requestType",
        x: 40,
        y: 104
      });
      deliveryCase__trackingNumber = new TextLayer({
        width: 278,
        height: inputBase.height,
        backgroundColor: inputBase.backgroundColor,
        text: inputBase.text,
        contentEditable: inputBase.contentEditable,
        lineHeight: inputBase.lineHeight,
        paddingLeft: inputBase.paddingLeft,
        fontSize: inputBase.fontSize,
        color: inputBase.color,
        superLayer: cases__item,
        name: "deliveryCase__trackingNumber",
        x: deliveryCase__requestType.x + deliveryCase__requestType.width + 82,
        y: deliveryCase__requestType.y + 230
      });
    } else {
      cases__content.height = 250;
      cases__content.backgroundColor = "beige";
    }
    cases__item.animate({
      properties: {
        height: 20 + cases__header.height + cases__content.height,
        opacity: 1
      },
      time: 0.8,
      curve: "ease-out"
    });
    cases__item.states.add({
      open: {
        height: 20 + cases__header.height + cases__content.height
      },
      close: {
        height: 60
      }
    });
    cases__header.onClick(function(event, layer) {
      var child, len3, o, ref5, results;
      layer.parent.states.next("close", "open");
      ref5 = layer.children;
      results = [];
      for (o = 0, len3 = ref5.length; o < len3; o++) {
        child = ref5[o];
        if (child.name === "caseCaret") {
          results.push(child.states.next("right", "down"));
        } else {
          results.push(void 0);
        }
      }
      return results;
    });
    casesArray = cases.children;
    results = [];
    for (o = 0, len3 = casesArray.length; o < len3; o++) {
      casesItem = casesArray[o];
      results.push(casesItem.on("change:height", function() {
        var j, p, ref5;
        for (i = p = 0, ref5 = casesArray.length; 0 <= ref5 ? p < ref5 : p > ref5; i = 0 <= ref5 ? ++p : --p) {
          j = i - 1;
          if (i === 0) {
            casesArray[i].y = 0;
          } else {
            casesArray[i].y = casesArray[j].y + casesArray[j].height;
          }
        }
        cases.height = updateHeight(cases);
        if (cases.height < 0) {
          cases.height = 0;
        }
        if (cases.children.length === 1 && cases.height < 40) {
          belowCases.y = cases.height + aboveCases.height + aboveCases.y;
        } else {
          belowCases.y = cases.y + cases.height;
        }
        if (updateHeight(mainContainerArray[0]) > mainContainerInitialHeight) {
          return mainContainerArray[0].height = updateHeight(mainContainerArray[0]);
        } else {
          return mainContainerArray[0].height = mainContainerInitialHeight;
        }
      }));
    }
    return results;
  });
}

Events.wrap(window).addEventListener("resize", function(event) {
  var m, ref3, results;
  page.width = Screen.width;
  page.height = Screen.height;
  results = [];
  for (i = m = 0, ref3 = numberOfPage - 1; 0 <= ref3 ? m <= ref3 : m >= ref3; i = 0 <= ref3 ? ++m : --m) {
    pageArray[i].width = Screen.width;
    pageArray[i].height = Screen.height;
    results.push(pageArray[i].x = Screen.width * i);
  }
  return results;
});


},{"./modules/TextLayer":2,"./modules/findModule":3}],2:[function(require,module,exports){
var TextLayer, convertTextLayers, convertToTextLayer,
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

TextLayer = (function(superClass) {
  extend(TextLayer, superClass);

  function TextLayer(options) {
    if (options == null) {
      options = {};
    }
    this.doAutoSize = false;
    this.doAutoSizeHeight = false;
    if (options.backgroundColor == null) {
      options.backgroundColor = options.setup ? "hsla(60, 90%, 47%, .4)" : "transparent";
    }
    if (options.color == null) {
      options.color = "red";
    }
    if (options.lineHeight == null) {
      options.lineHeight = 1.25;
    }
    if (options.fontFamily == null) {
      options.fontFamily = "Helvetica";
    }
    if (options.fontSize == null) {
      options.fontSize = 20;
    }
    if (options.text == null) {
      options.text = "Use layer.text to add text";
    }
    TextLayer.__super__.constructor.call(this, options);
    this.style.whiteSpace = "pre-line";
    this.style.outline = "none";
  }

  TextLayer.prototype.setStyle = function(property, value, pxSuffix) {
    if (pxSuffix == null) {
      pxSuffix = false;
    }
    this.style[property] = pxSuffix ? value + "px" : value;
    this.emit("change:" + property, value);
    if (this.doAutoSize) {
      return this.calcSize();
    }
  };

  TextLayer.prototype.calcSize = function() {
    var constraints, size, sizeAffectingStyles;
    sizeAffectingStyles = {
      lineHeight: this.style["line-height"],
      fontSize: this.style["font-size"],
      fontWeight: this.style["font-weight"],
      paddingTop: this.style["padding-top"],
      paddingRight: this.style["padding-right"],
      paddingBottom: this.style["padding-bottom"],
      paddingLeft: this.style["padding-left"],
      textTransform: this.style["text-transform"],
      borderWidth: this.style["border-width"],
      letterSpacing: this.style["letter-spacing"],
      fontFamily: this.style["font-family"],
      fontStyle: this.style["font-style"],
      fontVariant: this.style["font-variant"]
    };
    constraints = {};
    if (this.doAutoSizeHeight) {
      constraints.width = this.width;
    }
    size = Utils.textSize(this.text, sizeAffectingStyles, constraints);
    if (this.style.textAlign === "right") {
      this.width = size.width;
      this.x = this.x - this.width;
    } else {
      this.width = size.width;
    }
    return this.height = size.height;
  };

  TextLayer.define("autoSize", {
    get: function() {
      return this.doAutoSize;
    },
    set: function(value) {
      this.doAutoSize = value;
      if (this.doAutoSize) {
        return this.calcSize();
      }
    }
  });

  TextLayer.define("autoSizeHeight", {
    set: function(value) {
      this.doAutoSize = value;
      this.doAutoSizeHeight = value;
      if (this.doAutoSize) {
        return this.calcSize();
      }
    }
  });

  TextLayer.define("contentEditable", {
    set: function(boolean) {
      this._element.contentEditable = boolean;
      this.ignoreEvents = !boolean;
      return this.on("input", function() {
        if (this.doAutoSize) {
          return this.calcSize();
        }
      });
    }
  });

  TextLayer.define("text", {
    get: function() {
      return this._element.textContent;
    },
    set: function(value) {
      this._element.textContent = value;
      this.emit("change:text", value);
      if (this.doAutoSize) {
        return this.calcSize();
      }
    }
  });

  TextLayer.define("fontFamily", {
    get: function() {
      return this.style.fontFamily;
    },
    set: function(value) {
      return this.setStyle("fontFamily", value);
    }
  });

  TextLayer.define("fontSize", {
    get: function() {
      return this.style.fontSize.replace("px", "");
    },
    set: function(value) {
      return this.setStyle("fontSize", value, true);
    }
  });

  TextLayer.define("lineHeight", {
    get: function() {
      return this.style.lineHeight;
    },
    set: function(value) {
      return this.setStyle("lineHeight", value);
    }
  });

  TextLayer.define("fontWeight", {
    get: function() {
      return this.style.fontWeight;
    },
    set: function(value) {
      return this.setStyle("fontWeight", value);
    }
  });

  TextLayer.define("fontStyle", {
    get: function() {
      return this.style.fontStyle;
    },
    set: function(value) {
      return this.setStyle("fontStyle", value);
    }
  });

  TextLayer.define("fontVariant", {
    get: function() {
      return this.style.fontVariant;
    },
    set: function(value) {
      return this.setStyle("fontVariant", value);
    }
  });

  TextLayer.define("padding", {
    set: function(value) {
      this.setStyle("paddingTop", value, true);
      this.setStyle("paddingRight", value, true);
      this.setStyle("paddingBottom", value, true);
      return this.setStyle("paddingLeft", value, true);
    }
  });

  TextLayer.define("paddingTop", {
    get: function() {
      return this.style.paddingTop.replace("px", "");
    },
    set: function(value) {
      return this.setStyle("paddingTop", value, true);
    }
  });

  TextLayer.define("paddingRight", {
    get: function() {
      return this.style.paddingRight.replace("px", "");
    },
    set: function(value) {
      return this.setStyle("paddingRight", value, true);
    }
  });

  TextLayer.define("paddingBottom", {
    get: function() {
      return this.style.paddingBottom.replace("px", "");
    },
    set: function(value) {
      return this.setStyle("paddingBottom", value, true);
    }
  });

  TextLayer.define("paddingLeft", {
    get: function() {
      return this.style.paddingLeft.replace("px", "");
    },
    set: function(value) {
      return this.setStyle("paddingLeft", value, true);
    }
  });

  TextLayer.define("textAlign", {
    set: function(value) {
      return this.setStyle("textAlign", value);
    }
  });

  TextLayer.define("textTransform", {
    get: function() {
      return this.style.textTransform;
    },
    set: function(value) {
      return this.setStyle("textTransform", value);
    }
  });

  TextLayer.define("letterSpacing", {
    get: function() {
      return this.style.letterSpacing.replace("px", "");
    },
    set: function(value) {
      return this.setStyle("letterSpacing", value, true);
    }
  });

  TextLayer.define("length", {
    get: function() {
      return this.text.length;
    }
  });

  return TextLayer;

})(Layer);

convertToTextLayer = function(layer) {
  var css, cssObj, importPath, t;
  t = new TextLayer({
    name: layer.name,
    frame: layer.frame,
    parent: layer.parent
  });
  cssObj = {};
  css = layer._info.metadata.css;
  css.forEach(function(rule) {
    var arr;
    if (_.includes(rule, '/*')) {
      return;
    }
    arr = rule.split(': ');
    return cssObj[arr[0]] = arr[1].replace(';', '');
  });
  t.style = cssObj;
  importPath = layer.__framerImportedFromPath;
  if (_.includes(importPath, '@2x')) {
    t.fontSize *= 2;
    t.lineHeight = (parseInt(t.lineHeight) * 2) + 'px';
    t.letterSpacing *= 2;
  }
  t.y -= (parseInt(t.lineHeight) - t.fontSize) / 2;
  t.y -= t.fontSize * 0.1;
  t.x -= t.fontSize * 0.08;
  t.width += t.fontSize * 0.5;
  t.text = layer._info.metadata.string;
  layer.destroy();
  return t;
};

Layer.prototype.convertToTextLayer = function() {
  return convertToTextLayer(this);
};

convertTextLayers = function(obj) {
  var layer, prop, results;
  results = [];
  for (prop in obj) {
    layer = obj[prop];
    if (layer._info.kind === "text") {
      results.push(obj[prop] = convertToTextLayer(layer));
    } else {
      results.push(void 0);
    }
  }
  return results;
};

Layer.prototype.frameAsTextLayer = function(properties) {
  var t;
  t = new TextLayer;
  t.frame = this.frame;
  t.superLayer = this.superLayer;
  _.extend(t, properties);
  this.destroy();
  return t;
};

exports.TextLayer = TextLayer;

exports.convertTextLayers = convertTextLayers;


},{}],3:[function(require,module,exports){
var _findAll, _getHierarchy, _match;

_getHierarchy = function(layer) {
  var a, i, len, ref, string;
  string = '';
  ref = layer.ancestors();
  for (i = 0, len = ref.length; i < len; i++) {
    a = ref[i];
    string = a.name + '>' + string;
  }
  return string = string + layer.name;
};

_match = function(hierarchy, string) {
  var regExp, regexString;
  string = string.replace(/\s*>\s*/g, '>');
  string = string.split('*').join('[^>]*');
  string = string.split(' ').join('(?:.*)>');
  string = string.split(',').join('$|');
  regexString = "(^|>)" + string + "$";
  regExp = new RegExp(regexString);
  return hierarchy.match(regExp);
};

_findAll = function(selector, fromLayer) {
  var layers, stringNeedsRegex;
  layers = Framer.CurrentContext.getLayers();
  if (selector != null) {
    stringNeedsRegex = _.find(['*', ' ', '>', ','], function(c) {
      return _.includes(selector, c);
    });
    if (!(stringNeedsRegex || fromLayer)) {
      return layers = _.filter(layers, function(layer) {
        if (layer.name === selector) {
          return true;
        }
      });
    } else {
      return layers = _.filter(layers, function(layer) {
        var hierarchy;
        hierarchy = _getHierarchy(layer);
        if (fromLayer != null) {
          return _match(hierarchy, fromLayer.name + ' ' + selector);
        } else {
          return _match(hierarchy, selector);
        }
      });
    }
  } else {
    return layers;
  }
};

exports.Find = function(selector, fromLayer) {
  return _findAll(selector, fromLayer)[0];
};

exports.ƒ = function(selector, fromLayer) {
  return _findAll(selector, fromLayer)[0];
};

exports.FindAll = function(selector, fromLayer) {
  return _findAll(selector, fromLayer);
};

exports.ƒƒ = function(selector, fromLayer) {
  return _findAll(selector, fromLayer);
};

Layer.prototype.find = function(selector, fromLayer) {
  return _findAll(selector, this)[0];
};

Layer.prototype.ƒ = function(selector, fromLayer) {
  return _findAll(selector, this)[0];
};

Layer.prototype.findAll = function(selector, fromLayer) {
  return _findAll(selector, this);
};

Layer.prototype.ƒƒ = function(selector, fromLayer) {
  return _findAll(selector, this);
};


},{}]},{},[1]);
