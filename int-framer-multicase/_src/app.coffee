{TextLayer} = require './modules/TextLayer'
{ƒ,ƒƒ} = require './modules/findModule'


inputBgColor = "transparent"

inputBase = {}
inputBase.width = 292
inputBase.height = 21
inputBase.backgroundColor = inputBgColor
inputBase.text = ""
inputBase.contentEditable = true
inputBase.lineHeight = "21px"
inputBase.paddingLeft = 5
inputBase.fontSize = 14
inputBase.color = "#000"


# palette
palette = [
  '#f2eeb1', # [0] pale yellow
  '#e1edce', # [1] pale green
  '#b7d0c1', # [2] light green
  '#8ab3b3', # [3] aqua green
  '#668391', # [4] blue-ish green
  '#3f5270', # [5] blue denim
  '#6e4c60', # [6] purple
  '#9d4651', # [7] maroon purple
  '#cf3e40', # [8] firy red
  '#d66b5f', # [9] muted red
  '#df967c', # [10] salmon orange
  '#e8c198' # [11] pale orange
]

# define a page component
page = new PageComponent
  width: Screen.width
  height: Screen.height
  scrollVertical: false
  backgroundColor: 'white'

# disable draggable page
page.content.draggable.enabled = false


# set the number of page required for the prototype (not necessarily screens)
numberOfPage = 1


# array definition to store and target pages and containers
pageArray = []
mainContainerArray = []


mainContainerInitialHeight = 800
# loop to create the pages, the containers and any other common elements
for i in [0..numberOfPage-1]
  # create the page layers that are linked to the page component
  pageItem = new Layer
    parent: page.content
    width: Screen.width
    height: Screen.height
    x: Screen.width * i
    backgroundColor: "#fff"
    name: i

  pageItem.scroll = true
  pageItem.scrollHorizontal = false
  pageItem.mouseWheelEnabled = true

  pageArray.push(pageItem)



  # create the main containers for each page
  mainContainer = new Layer
    width: 985
    height: mainContainerInitialHeight
    backgroundColor: 'white'
    superLayer: pageArray[i]
    y: 0


  mainContainer.style =
    width: '985px',
    marginLeft: 'calc((100% - 985px) / 2)'

  # make the container scrollable
  mainContainer.scroll = true
  mainContainer.scrollHorizontal = false
  mainContainer.mouseWheelEnabled = true

  mainContainerArray.push(mainContainer)


structure = new Layer
  superLayer: mainContainerArray[0]
  width: mainContainerArray[0].width
  height: 1178

structure.style =
  "background": "url('img/structure.png') 50% 0 no-repeat"

updateHeight = (layer) ->
  if layer == mainContainerArray[0]
    layerHeight = layer.contentFrame().height + 60
  else
    layerHeight = layer.contentFrame().height
  return layerHeight

myFunction = (input) ->
    output = input * 2
    return output


aboveCases = new Layer
  superLayer: mainContainerArray[0]
  width: 756
  height: 589
  image: "img/multi-case-header.png"
  x: 211
  y: 167

# layer that contains editable fields for the multi case details
bundleDetails = new Layer
  superLayer: aboveCases
  backgroundColor: "transparent"
  width: aboveCases.width

# reference field
bundleDetails__ref = new TextLayer
  width: inputBase.width
  height: inputBase.height
  backgroundColor: inputBase.backgroundColor
  text: inputBase.text
  contentEditable: inputBase.contentEditable
  lineHeight: inputBase.lineHeight
  paddingLeft: inputBase.paddingLeft
  fontSize: inputBase.fontSize
  color: inputBase.color 
  superLayer: bundleDetails
  x: 23
  y: 269

# summary field
bundleDetails__summary = new TextLayer
  width: inputBase.width
  height: inputBase.height
  backgroundColor: inputBase.backgroundColor
  text: inputBase.text
  contentEditable: inputBase.contentEditable
  lineHeight: inputBase.lineHeight
  paddingLeft: inputBase.paddingLeft
  fontSize: inputBase.fontSize
  color: inputBase.color 
  superLayer: bundleDetails
  superLayer: bundleDetails
  x: bundleDetails__ref.x
  y: bundleDetails__ref.y + bundleDetails__ref.height + 39




# layer that contains the cases
cases = new Layer
  superLayer: mainContainerArray[0]
  width: 750
  height: 0
  backgroundColor: "transparent"
  name: "casesLayer"
  y: aboveCases.height + aboveCases.y
  x: 213



# layer that contains elements below the cases layer
# allows to change the y position more easily
belowCases = new Layer
  superLayer: mainContainerArray[0]
  width: 750
  backgroundColor: "transparent"
  y: cases.height + cases.y
  x: 213


# layer that contains the buttons to create cases
caseBtns = new Layer
  superLayer: belowCases
  width: 750
  height: 78
  y: 20
  backgroundColor: "#e6f1f4"
  borderColor: "#ccddec"
  borderWidth: 1
  borderRadius: 5

saveMultiArea = new Layer
  superLayer: belowCases
  height: 200
  backgroundColor: "transparent"
  width: belowCases.width
  y: caseBtns.height + caseBtns.y + 20

saveMultiBtn = new TextLayer
  superLayer: saveMultiArea
  text: "save multi-case"
  image: "img/btn-fat-empty.png"
  width: 327
  height: 40
  lineHeight: "40px"
  color: "#fff"
  textAlign: "center"
  x: Align.right
  opacity: 0.4

saveMultiBtn.states.add
  enabled:
    opacity: 1




belowCases.height = updateHeight(belowCases)


caseBtns__title = new TextLayer
  superLayer: caseBtns
  text: "Open a case"
  height: 20
  color: "#000"
  fontSize: 18
  x: 20
  y: 5


# hand and eyes button layer
caseBtns__item_hands = new Layer
  superLayer: caseBtns
  image: "img/btn-hands.png"
  width: 114
  height: 24
  x: 20
  y: 34
  name: "caseBtn__hands"


# temporary access button layer
caseBtns__item_access = new Layer
  superLayer: caseBtns
  image: "img/btn-access.png"
  width: 136
  height: 24
  x: caseBtns__item_hands.x + caseBtns__item_hands.width + 20
  y: 34
  name: "caseBtn__access"


# delivery and removal button layer
caseBtns__item_delivery = new Layer
  superLayer: caseBtns
  image: "img/btn-delivery.png"
  width: 155
  height: 24
  x: caseBtns__item_access.x + caseBtns__item_access.width + 20
  y: 34
  name: "caseBtn__delivery"



for btn in caseBtns.children
  btn.onClick (event, btn) ->
    # collapse existing cases when a new one is added
    for layer in cases.children
      layer.states.switch("close")
      for caret in layer.ƒƒ("caseCaret")
        caret.states.switch("right")

    # main layer for a single case
    cases__item = new Layer
      superLayer: cases
      width: cases.width
      opacity: 0
      name: "caseItem"
      backgroundColor: "transparent"
      borderRadius: 5

    cases__item.height = 0
    cases__item.clip = true

    # header of a single case
    cases__header = new Layer
      superLayer: cases__item
      height: 40
      y: 20
      width: cases__item.width
      image: "img/header-bg.png"

    # part of header : caret rotating icon
    cases__caret = new Layer
      superLayer: cases__header
      width: 5
      height: 10
      image: "img/icon-caret-right.png"
      x: 13
      name: "caseCaret"
      rotation: 90

    cases__caret.centerY()

    cases__caret.states.add
      right:
        rotation: 0
      down:
        rotation: 90

    cases__caret.states.animationOptions =
      time: 0.3

    # part of header : icon for the type of case
    cases__icon = new Layer
      superLayer: cases__header
      width: 20
      height: 20
      x: 25

    cases__icon.centerY()

    # part of header : name of the case
    cases__title = new TextLayer
      superLayer: cases__header
      text: "Case Title Here"
      color: "#000"
      fontSize: 18
      autoSize: true
      height: 20
      y: 8
      x: 48


    # remove buttons layers with confirm
    cases__remove = new Layer
      superLayer: cases__item
      width: 120
      height: 40
      image: "img/remove.png"
      x: Align.right
      y: 20
      name: "remove area"
    
    cases__remove.clip = true

    cases__remove.states.add
      extended:
        width: 240
        x: cases__remove.x - 120

    cases__remove.states.animationOptions =
      curve: "ease-in-out"
      time: 0.2

    cases__removeBtnsSet = new Layer
      superLayer: cases__remove
      width: 2 * cases__remove.width
      backgroundColor: "transparent"
      x: cases__remove.width

    cases__removeBtnsSet.states.add
      show:
        x: 0

    cases__removeBtnsSet.states.animationOptions =
      curve: "ease-in-out"
      time: 0.2

    cases__removeConfirm = new Layer
      superLayer: cases__removeBtnsSet
      width: cases__remove.width
      height: cases__remove.height
      image: "img/remove-confirm.png"
      x: 0

    cases__remove.onClick (event, layer) ->
      layer.states.next()
      for btn in layer.children
        btn.states.next()

    cases__removeConfirm.onClick (event,layer) ->
      layer.parent.parent.parent.animate
        properties:
          height: 0
          opacity: 0

      layer.parent.parent.parent.on Events.AnimationEnd, (animation, layer) ->
        layer.destroy()

    cases__removeCancel = new Layer
      superLayer: cases__removeBtnsSet
      width: cases__remove.width
      height: cases__remove.height
      image: "img/remove-cancel.png"
      x: cases__remove.width


    # layer that contains the details of the case
    cases__content = new Layer
      superLayer: cases__item
      width: cases__item.width
      backgroundColor: "transparent"
      y: 60

    # part of content : save button
    cases__saveBtn = new Layer
      superLayer: cases__content
      width: 327
      height: 40
      image: "img/btn-inside-case.png"
      x: 403

    cases__saveBtn.onClick (event, layer) ->
      layer.parent.parent.states.switch("close")

      for icon in layer.parent.parent.ƒƒ("caseCaret")
        icon.states.switch("right")


    # display different layouts or details depending on which type of cases is added 
    if btn.name == "caseBtn__hands"
      cases__content.height = 558
      cases__content.image = "img/hands-content.png"
      cases__content.name = "caseHandsContent"
      cases__saveBtn.y = 472
      cases__icon.image = "img/icon-eye.png"
      cases__title.text = "Hands And Eyes"

      handCase__phone = new TextLayer
        width: 278
        height: inputBase.height
        backgroundColor: inputBase.backgroundColor
        text: inputBase.text
        contentEditable: inputBase.contentEditable
        lineHeight: inputBase.lineHeight
        paddingLeft: inputBase.paddingLeft
        fontSize: inputBase.fontSize
        color: inputBase.color 
        superLayer: cases__item
        name: "handCase__phone"
        x: 40
        y: 114

      handCase__requestType = new TextLayer
        width: 278
        height: inputBase.height
        backgroundColor: inputBase.backgroundColor
        text: inputBase.text
        contentEditable: inputBase.contentEditable
        lineHeight: inputBase.lineHeight
        paddingLeft: inputBase.paddingLeft
        fontSize: inputBase.fontSize
        color: inputBase.color 
        superLayer: cases__item
        name: "handCase__requestType"
        x: handCase__phone.x + handCase__phone.width + 82
        y: 114

    else if btn.name == "caseBtn__access"
      cases__content.height = 718
      cases__content.image = "img/access-content.png"
      cases__content.name = "caseAccessContent"
      cases__saveBtn.y = 635
      cases__icon.image = "img/icon-access.png"
      cases__title.text = "Temporary Access"

    else if btn.name == "caseBtn__delivery"
      cases__content.height = 609
      cases__content.image = "img/delivery-content.png"
      cases__content.name = "caseDeliveryContent"
      cases__saveBtn.y = 524
      cases__icon.image = "img/icon-delivery.png"
      cases__title.text = "Delivery And Removal"

      deliveryCase__requestType = new TextLayer
        width: 278
        height: inputBase.height
        backgroundColor: inputBase.backgroundColor
        text: inputBase.text
        contentEditable: inputBase.contentEditable
        lineHeight: inputBase.lineHeight
        paddingLeft: inputBase.paddingLeft
        fontSize: inputBase.fontSize
        color: inputBase.color 
        superLayer: cases__item
        name: "deliveryCase__requestType"
        x: 40
        y: 104

      deliveryCase__trackingNumber = new TextLayer
        width: 278
        height: inputBase.height
        backgroundColor: inputBase.backgroundColor
        text: inputBase.text
        contentEditable: inputBase.contentEditable
        lineHeight: inputBase.lineHeight
        paddingLeft: inputBase.paddingLeft
        fontSize: inputBase.fontSize
        color: inputBase.color 
        superLayer: cases__item
        name: "deliveryCase__trackingNumber"
        x: deliveryCase__requestType.x + deliveryCase__requestType.width + 82 
        y: deliveryCase__requestType.y + 230

    else
      cases__content.height = 250
      cases__content.backgroundColor = "beige"

    # animate the single case when it's created
    cases__item.animate
      properties:
        height: 20 + cases__header.height + cases__content.height
        opacity: 1
      time: 0.8
      curve: "ease-out"

    cases__item.states.add
      open: 
        height: 20 + cases__header.height + cases__content.height
      close:
        height: 60

    # collapse the content and rotate the caret when clicking on the header
    cases__header.onClick (event,layer) ->
      layer.parent.states.next("close", "open")
      for child in layer.children
        if child.name == "caseCaret"
          child.states.next("right","down")

    casesArray = cases.children

    for casesItem in casesArray     
      # rearrange layers when a case's height is changed or a case is removed
      casesItem.on "change:height", ->
        for i in [0...casesArray.length]
          j = i - 1
          if i == 0
            casesArray[i].y = 0
          else
            casesArray[i].y = casesArray[j].y + casesArray[j].height
        
        cases.height = updateHeight(cases)
        if cases.height < 0
          cases.height = 0

        if cases.children.length == 1 and cases.height < 40
          belowCases.y = cases.height + aboveCases.height + aboveCases.y
        else
          belowCases.y = cases.y + cases.height
          
        if updateHeight(mainContainerArray[0]) > mainContainerInitialHeight
          mainContainerArray[0].height = updateHeight(mainContainerArray[0])
        else
          mainContainerArray[0].height = mainContainerInitialHeight


# resize update : probably not good for performance
Events.wrap(window).addEventListener "resize", (event) ->
  page.width = Screen.width
  page.height = Screen.height

  for i in [0..numberOfPage-1]
    pageArray[i].width = Screen.width
    pageArray[i].height = Screen.height
    pageArray[i].x = Screen.width * i






# example to use layers defined in an external file
# module = require './modules/test'
# module.mylayerA.props =
#   x: 0
#   y: 100
# module.mylayerA.superLayer = mainContainerArray[1]





# # loop to generate some random bloc
# for i in [0..5]
#   blocItem = new Layer
#     superLayer: pageArray[0]
#     width: 200
#     height: 500
#     backgroundColor: Utils.randomChoice(palette)
#     y: 505 * i + 5

#   blocItem.centerX()